<?php
declare(strict_types=1);
/*
 * This file is part of "irstea/ng-model-generator-bundle".
 *
 * "irstea/ng-model-generator-bundle" generates Typescript interfaces for Angular using api-platform metadata.
 * Copyright (C) 2018-2021 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\NgModelGeneratorBundle\Tests\Iterators;

use Irstea\NgModelGeneratorBundle\Iterators\RecursorIterator;
use PHPUnit\Framework\TestCase;
use Prophecy\Prophecy\ObjectProphecy;

/**
 * Class RecursorIteratorTest.
 */
class RecursorIteratorTest extends TestCase
{
    /** @var RecursorIterator */
    private $tested;

    protected function setUp(): void/* The :void return type declaration that should be here would cause a BC issue */
    {
        parent::setUp();

        /** @var ObjectProphecy|\IteratorAggregate $agg */
        $agg = $this->prophesize(\IteratorAggregate::class);
        $agg->getIterator()->willReturn(new \ArrayIterator([8]));

        $inner = new \ArrayIterator([5, [6], $agg->reveal()]);

        $this->tested = new RecursorIterator($inner);
    }

    public function testHasChildren(): void
    {
        $this->tested->rewind();
        self::assertFalse($this->tested->hasChildren());

        $this->tested->next();
        self::assertTrue($this->tested->hasChildren());

        $this->tested->next();
        self::assertTrue($this->tested->hasChildren());

        $this->tested->next();
        self::assertFalse($this->tested->valid());
    }

    public function testGetChildren(): void
    {
        $this->tested->rewind();
        self::assertNull($this->tested->getChildren());

        $this->tested->next();
        self::assertEquals([6], iterator_to_array($this->tested->getChildren()));

        $this->tested->next();
        self::assertEquals([8], iterator_to_array($this->tested->getChildren()));

        $this->tested->next();
        self::assertFalse($this->tested->valid());
    }
}
