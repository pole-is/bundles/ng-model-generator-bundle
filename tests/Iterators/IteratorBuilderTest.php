<?php
declare(strict_types=1);
/*
 * This file is part of "irstea/ng-model-generator-bundle".
 *
 * "irstea/ng-model-generator-bundle" generates Typescript interfaces for Angular using api-platform metadata.
 * Copyright (C) 2018-2021 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\NgModelGeneratorBundle\Tests\Iterators;

use Irstea\NgModelGeneratorBundle\Iterators\IteratorBuilder;
use PHPUnit\Framework\TestCase;
use Prophecy\Prophecy\ObjectProphecy;

/**
 * Class IteratorBuilderTest.
 */
final class IteratorBuilderTest extends TestCase
{
    public function testIterateFromArray(): void
    {
        self::assertSame(
            [1, 2, 3],
            self::toArray(
                IteratorBuilder::from([1, 2, 3])
            )
        );
    }

    public function testIterateFromIterator(): void
    {
        self::assertSame(
            [1, 2, 3],
            self::toArray(
                IteratorBuilder::from(new \ArrayIterator([1, 2, 3]))
            )
        );
    }

    public function testIterateFromIteratorAggregate(): void
    {
        /** @var \IteratorAggregate|ObjectProphecy $agg */
        $agg = $this->prophesize(\IteratorAggregate::class);
        $agg->getIterator()->willReturn(new \ArrayIterator([1, 2, 3]));

        self::assertSame(
            [1, 2, 3],
            self::toArray(
                IteratorBuilder::from($agg->reveal())
            )
        );
    }

    public function testUniqueIteration(): void
    {
        $a = new \stdClass();
        $b = new \stdClass();

        self::assertSame(
            [1, 2, $a, 3, $b, 4],
            self::toArray(
                IteratorBuilder::from([1, 2, 1, $a, $a, 3, $b, 4, $a])
                ->unique()
            )
        );
    }

    public function testIteratorRecursiveIteration(): void
    {
        $inner = new \ArrayIterator([1, 2, 3]);

        self::assertSame(
            [0, 1, 2, 3, 4],
            self::toArray(
                IteratorBuilder::from([0, $inner, 4])
                ->recurse(\RecursiveIteratorIterator::LEAVES_ONLY)
            )
        );
    }

    public function testArrayRecursiveIteration(): void
    {
        self::assertSame(
            [0, 1, 2, 3, 4],
            self::toArray(
                IteratorBuilder::from([0, [1, 2, 3], 4])
                ->recurse(\RecursiveIteratorIterator::LEAVES_ONLY)
            )
        );
    }

    public function testRecursiveUniqueIteration(): void
    {
        /** @var \IteratorAggregate|ObjectProphecy $inner */
        $inner = $this->prophesize(\IteratorAggregate::class);
        $inner->getIterator()
            ->willReturn(new \ArrayIterator([1, 2, 3]))
            ->shouldBeCalledTimes(1);

        self::assertSame(
            [0, 1, 2, 3, 4],
            self::toArray(
                IteratorBuilder::from([0, 1, $inner->reveal(), $inner->reveal(), 0, 4])
                ->recurse(\RecursiveIteratorIterator::LEAVES_ONLY)
                ->unique()
            )
        );
    }

    public function testFilteringIteration(): void
    {
        self::assertSame(
            [1, 3],
            self::toArray(
                IteratorBuilder::from([0, 1, 2, 3, 4])
                ->where(function ($value) {
                    return $value % 2;
                })
            )
        );
    }

    public function testRecursiveFilteringIteration(): void
    {
        self::assertSame(
            [1, 5, 7, 3],
            self::toArray(
                IteratorBuilder::from([0, 1, new \ArrayIterator([5, 6, 7]), 3, 4])
                ->where(function ($value) {
                    return $value % 2;
                })
                ->recurse(\RecursiveIteratorIterator::LEAVES_ONLY)
            )
        );
    }

    public function testRecursiveUniqueFilteringIteration(): void
    {
        self::assertSame(
            [1, 5, 7, 3],
            self::toArray(
                IteratorBuilder::from([0, 1, new \ArrayIterator([1, 5, 6, 7]), 7, 3, 4])
                ->where(function ($value) {
                    return $value % 2;
                })
                ->unique()
                ->recurse(\RecursiveIteratorIterator::LEAVES_ONLY)
            )
        );
    }

    public function testConditionnallyRecursiveIteration(): void
    {
        $inner = new \ArrayIterator([2, 3, 4]);
        $input = [0, 1, $inner, ['foo', 'bar', $inner], 5];

        self::assertSame(
            [0, 1, $inner, ['foo', 'bar', $inner], 'foo', 'bar', $inner, 5],
            self::toArray(
                IteratorBuilder::from($input)
                ->recurseWhere(
                    \RecursiveIteratorIterator::SELF_FIRST,
                    function ($v) {
                        return \is_array($v);
                    }
                )
            )
        );
    }

    public function testModifierOrderDoesNotMatter(): void
    {
        $input = [0, 1, new \ArrayIterator([1, 5, 6, 7]), 7, 3, 4];

        self::assertSame(
            self::toArray(
                IteratorBuilder::from($input)
                ->recurse(\RecursiveIteratorIterator::LEAVES_ONLY)
                ->unique()
                ->where(function ($value) {
                    return $value % 2;
                })
            ),
            self::toArray(
                IteratorBuilder::from($input)
                ->where(function ($value) {
                    return $value % 2;
                })
                ->unique()
                ->recurse(\RecursiveIteratorIterator::LEAVES_ONLY)
            )
        );
    }

    private static function toArray(\Traversable $iter): array
    {
        $ary = [];
        foreach ($iter as $value) {
            $ary[] = $value;
        }

        return $ary;
    }
}
