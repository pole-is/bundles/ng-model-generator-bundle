<?php
declare(strict_types=1);
/*
 * This file is part of "irstea/ng-model-generator-bundle".
 *
 * "irstea/ng-model-generator-bundle" generates Typescript interfaces for Angular using api-platform metadata.
 * Copyright (C) 2018-2021 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\NgModelGeneratorBundle\Tests\Iterators;

use Irstea\NgModelGeneratorBundle\Iterators\UniqueFilter;
use PHPUnit\Framework\TestCase;

/**
 * Class UniqueFilterTest.
 */
class UniqueFilterTest extends TestCase
{
    /** @var UniqueFilter */
    private $tested;

    protected function setUp(): void/* The :void return type declaration that should be here would cause a BC issue */
    {
        parent::setUp();

        $this->tested = new UniqueFilter();
    }

    public function testKeepUniqueValues(): void
    {
        $input = [4, 5, null, new \stdClass(), new \stdClass()];
        self::assertEquals($input, array_filter($input, $this->tested));
    }

    public function testRemoveDuplicates(): void
    {
        $obj = new \stdClass();

        $input = [4, 5, null, 4, $obj, 8, $obj, 4, 5];
        $actual = array_values(array_filter($input, $this->tested));
        $expected = [4, 5, null, $obj, 8];

        self::assertEquals(\count($expected), \count($actual));

        foreach ($expected as $i => $value) {
            self::assertSame($value, $actual[$i]);
        }
    }
}
