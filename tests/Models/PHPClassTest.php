<?php
declare(strict_types=1);
/*
 * This file is part of "irstea/ng-model-generator-bundle".
 *
 * "irstea/ng-model-generator-bundle" generates Typescript interfaces for Angular using api-platform metadata.
 * Copyright (C) 2018-2021 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\NgModelGeneratorBundle\Tests\Models;

use Irstea\NgModelGeneratorBundle\Models\PHPClass;
use PHPUnit\Framework\TestCase;

/**
 * Class PHPClassTest.
 */
class PHPClassTest extends TestCase
{
    /**
     * @param $expected
     * @param $fqcn
     *
     * @dataProvider getBaseNameTestCases
     */
    public function testGetBaseName(string $expected, string $fqcn): void
    {
        $class = PHPClass::get($fqcn);
        self::assertEquals($expected, $class->getBaseName());
    }

    public function getBaseNameTestCases(): array
    {
        return [
            ['Foo', 'Foo'],
            ['stdClass', \stdClass::class],
            ['PHPClass', PHPClass::class],
        ];
    }

    /**
     * @param $expected
     * @param $fqcn
     *
     * @dataProvider getNamespaceTestCases
     */
    public function testGetNamespace(string $expected, string $fqcn): void
    {
        $class = PHPClass::get($fqcn);
        self::assertEquals($expected, $class->getNamespace());
    }

    public function getNamespaceTestCases(): array
    {
        return [
            ['', 'Foo'],
            ['', \stdClass::class],
            ['Irstea\\NgModelGeneratorBundle\\Models\\', PHPClass::class],
        ];
    }

    /**
     * @param $expected
     * @param $fqcn
     *
     * @dataProvider getFullNameTestCases
     */
    public function testGetFullName(string $fqcn): void
    {
        $class = PHPClass::get($fqcn);
        self::assertEquals($fqcn, $class->getFullName());
    }

    public function getFullNameTestCases(): array
    {
        return [
            ['Foo'],
            [\stdClass::class],
            [PHPClass::class],
        ];
    }
}
