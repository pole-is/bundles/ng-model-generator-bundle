<?php
declare(strict_types=1);
/*
 * This file is part of "irstea/ng-model-generator-bundle".
 *
 * "irstea/ng-model-generator-bundle" generates Typescript interfaces for Angular using api-platform metadata.
 * Copyright (C) 2018-2021 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\NgModelGeneratorBundle\Tests\Models;

use Irstea\NgModelGeneratorBundle\Models\ClassInfo;
use Irstea\NgModelGeneratorBundle\Models\HasName;
use Irstea\NgModelGeneratorBundle\Models\PHPClass;
use PHPUnit\Framework\TestCase;

/**
 * Class ClassInfoTest.
 */
final class ClassInfoTest extends TestCase
{
    public function testShouldBeCreated(): void
    {
        $c = PHPClass::get('\\namespace\\bla');
        $ci = $this->create('\\namespace\\bla', ['id', 'name'], false);

        self::assertEquals($c->getFullName(), $ci->getFullName());
        self::assertEquals($c->getBaseName(), $ci->getBaseName());
        self::assertEquals($c->getNamespace(), $ci->getNamespace());
        self::assertFalse($ci->isAbstract());
        self::assertArrayKeys(['id', 'name'], $ci->getConcreteProperties());
        self::assertArrayKeys(['id', 'name'], $ci->getVirtualProperties());
    }

    /**
     * @expectedException \Irstea\NgModelGeneratorBundle\Exceptions\DomainException
     */
    public function testCannotBeItsOwnParent(): void
    {
        $ci = $this->create('\\namespace\\bla', ['id', 'name'], false);
        $ci->setParent($ci);
    }

    /**
     * @expectedException \Irstea\NgModelGeneratorBundle\Exceptions\DomainException
     */
    public function testSelfCannotBeReparented(): void
    {
        $ci = $this->create('\\namespace\\bla', ['id', 'name'], false);
        $parent = $this->create('\\namespace\\bar');
        $otherParent = $this->create('\\namespace\\quz');

        $ci->setParent($parent);
        $ci->setParent($otherParent);
    }

    public function testRearrangeSingleClass(): void
    {
        $ci = $this->create('\\namespace\\bla', ['id', 'name'], false);

        $ci->rearrangeHiearchy();

        self::assertTrue($ci->isInterface(), 'ClassInfo is an interface');
        self::assertArrayKeys(['id', 'name'], $ci->getConcreteProperties());
        self::assertArrayKeys(['id', 'name'], $ci->getVirtualProperties());
    }

    public function testRearrangeSingleAbstractClass(): void
    {
        $ci = $this->create('\\namespace\\bla', ['id', 'name'], true);

        $ci->rearrangeHiearchy();

        self::assertTrue($ci->isIRI(), 'ClassInfo is an IRI');
    }

    public function testRearrangeTwoLevelHierarchy(): void
    {
        $root = $this->create('\\namespace\\foo', ['id', 'name'], false);

        $child1 = $this->create('\\namespace\\bar', ['id', 'level', 'volume'], false);
        $child1->setParent($root);

        $child2 = $this->create('\\namespace\\bar', ['id', 'length', 'volume'], false);
        $child2->setParent($root);

        $root->rearrangeHiearchy();

        self::assertTrue($root->isInterface(), 'root is an interface');
        self::assertTrue($child1->isInterface(), 'child1 is an interface');
        self::assertTrue($child2->isInterface(), 'child2 is an interface');

        self::assertArrayKeys(['id', 'name'], $root->getConcreteProperties());
        self::assertArrayKeys(['id', 'name'], $root->getVirtualProperties());

        self::assertArrayKeys(['level', 'volume'], $child1->getConcreteProperties());
        self::assertArrayKeys(['id', 'name', 'level', 'volume'], $child1->getVirtualProperties());

        self::assertArrayKeys(['length', 'volume'], $child2->getConcreteProperties());
        self::assertArrayKeys(['id', 'name', 'length', 'volume'], $child2->getVirtualProperties());
    }

    public function testRearrangeTwoLevelHierarchyWithAbstractRoot(): void
    {
        $root = $this->create('\\namespace\\foo', ['id', 'name'], true);

        $child1 = $this->create('\\namespace\\bar', ['id', 'level', 'volume'], false);
        $child1->setParent($root);

        $child2 = $this->create('\\namespace\\bar', ['id', 'length', 'volume'], false);
        $child2->setParent($root);

        $root->rearrangeHiearchy();

        self::assertTrue($root->isUnion(), 'root is an union');
        self::assertTrue($child1->isInterface(), 'child1 is an interface');
        self::assertTrue($child2->isInterface(), 'child2 is an interface');

        self::assertArrayKeys([], $root->getConcreteProperties());
        self::assertArrayKeys(['id', 'name', 'volume'], $root->getVirtualProperties());

        self::assertArrayKeys(['id', 'name', 'level', 'volume'], $child1->getConcreteProperties());
        self::assertArrayKeys(['id', 'name', 'level', 'volume'], $child1->getVirtualProperties());

        self::assertArrayKeys(['id', 'name', 'length', 'volume'], $child2->getConcreteProperties());
        self::assertArrayKeys(['id', 'name', 'length', 'volume'], $child2->getVirtualProperties());
    }

    public function testRearrangeThreeLevelHierarchy(): void
    {
        $root = $this->create('\\namespace\\foo', ['id', 'name'], false);

        $child1 = $this->create('\\namespace\\bar', ['id', 'level', 'volume'], false);
        $child1->setParent($root);

        $child2 = $this->create('\\namespace\\bar', ['id', 'length', 'volume'], false);
        $child2->setParent($root);

        $grandChild1 = $this->create('\\namespace\\bar', ['id', 'level', 'thing'], false);
        $grandChild1->setParent($child1);

        $root->rearrangeHiearchy();

        self::assertTrue($root->isInterface(), 'root is an interface');
        self::assertTrue($child1->isInterface(), 'child1 is an interface');
        self::assertTrue($child2->isInterface(), 'child2 is an interface');

        self::assertArrayKeys(['id', 'name'], $root->getConcreteProperties());
        self::assertArrayKeys(['id', 'name'], $root->getVirtualProperties());

        self::assertArrayKeys(['level', 'volume'], $child1->getConcreteProperties());
        self::assertArrayKeys(['id', 'name', 'level', 'volume'], $child1->getVirtualProperties());

        self::assertArrayKeys(['length', 'volume'], $child2->getConcreteProperties());
        self::assertArrayKeys(['id', 'name', 'length', 'volume'], $child2->getVirtualProperties());

        self::assertArrayKeys(['thing'], $grandChild1->getConcreteProperties());
        self::assertArrayKeys(['id', 'name', 'level', 'volume', 'thing'], $grandChild1->getVirtualProperties());
    }

    public function testRearrangeEmptySingleChild(): void
    {
        $root = $this->create('\\namespace\\foo', ['id', 'name'], false);

        $child1 = $this->create('\\namespace\\bar', ['id', 'name'], false);
        $child1->setParent($root);

        $root->rearrangeHiearchy();

        self::assertTrue($root->isInterface(), 'root is an interface');
        self::assertTrue($child1->isIRI(), 'child1 is an IRI');

        self::assertArrayKeys(['id', 'name'], $root->getConcreteProperties());
        self::assertArrayKeys(['id', 'name'], $root->getVirtualProperties());

        self::assertArrayKeys([], $child1->getConcreteProperties());
        self::assertArrayKeys(['id', 'name'], $child1->getVirtualProperties());
    }

    public function testRearrangeEmptyChildAbstractParent(): void
    {
        $root = $this->create('\\namespace\\foo', ['id', 'name'], true);

        $child1 = $this->create('\\namespace\\bar', [], false);
        $child1->setParent($root);

        $root->rearrangeHiearchy();

        self::assertTrue($root->isUnion(), 'root is an union');
        self::assertTrue($child1->isInterface(), 'child1 is an interface');

        self::assertArrayKeys([], $root->getConcreteProperties());
        self::assertArrayKeys(['id', 'name'], $root->getVirtualProperties());

        self::assertArrayKeys(['id', 'name'], $child1->getConcreteProperties());
        self::assertArrayKeys(['id', 'name'], $child1->getVirtualProperties());
    }

    private function create(string $className, array $propertyNames = [], bool $abstract = false): ClassInfo
    {
        return new ClassInfo(
            PHPClass::get($className),
            array_map(
                function ($name) {
                    $prop = $this->prophesize(HasName::class);
                    $prop->getName()->willReturn($name);

                    return $prop->reveal();
                },
                $propertyNames
            ),
            $abstract
        );
    }

    /**
     * @param string[]    $expected
     * @param string|null $message
     */
    public static function assertArrayKeys(array $expected, array $actual): void
    {
        sort($expected);
        $keys = array_keys($actual);
        sort($keys);
        self::assertSame($expected, $keys);
    }
}
