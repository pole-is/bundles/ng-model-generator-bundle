<?php
declare(strict_types=1);
/*
 * This file is part of "irstea/ng-model-generator-bundle".
 *
 * "irstea/ng-model-generator-bundle" generates Typescript interfaces for Angular using api-platform metadata.
 * Copyright (C) 2018-2021 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\NgModelGeneratorBundle\Tests\Fixtures\Entity;

use ApiPlatform\Core\Annotation as API;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

/**
 * Class EntityWithAccessors.
 *
 * @API\ApiResource()
 */
class EntityWithAccessors
{
    /**
     * @var UuidInterface
     * @API\ApiProperty(identifier=true)
     */
    private $id;

    /**
     * @var string
     */
    private $initRead;

    /**
     * @var string
     */
    private $initOnly;

    /**
     * @var string
     */
    private $full;

    /**
     * @var string
     */
    private $writeOnly;

    /**
     * @var string
     */
    private $readOnly;

    /**
     * EntityWithAccessors constructor.
     */
    public function __construct(string $initRead, string $initOnly, string $full)
    {
        $this->initRead = $initRead;
        $this->initOnly = $initOnly;
        $this->full = $full;
        $this->readOnly = 'foobar';
        $this->id = Uuid::uuid4();
    }

    /**
     * Get id.
     */
    public function getId(): UuidInterface
    {
        return $this->id;
    }

    /**
     * Get initRead.
     */
    public function getInitRead(): string
    {
        return $this->initRead;
    }

    /**
     * Get readOnly.
     */
    public function getReadOnly(): string
    {
        return $this->readOnly;
    }

    /**
     * Get full.
     */
    public function getFull(): string
    {
        return $this->full;
    }

    /**
     * Set full.
     */
    public function setFull(string $full): void
    {
        $this->full = $full;
    }

    /**
     * Set writeOnly.
     */
    public function setWriteOnly(string $writeOnly): void
    {
        $this->writeOnly = $writeOnly;
    }
}
