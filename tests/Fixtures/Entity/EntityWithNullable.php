<?php
declare(strict_types=1);
/*
 * This file is part of "irstea/ng-model-generator-bundle".
 *
 * "irstea/ng-model-generator-bundle" generates Typescript interfaces for Angular using api-platform metadata.
 * Copyright (C) 2018-2021 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\NgModelGeneratorBundle\Tests\Fixtures\Entity;

use ApiPlatform\Core\Annotation as API;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

/**
 * Class EntityWithNullable.
 *
 * @API\ApiResource()
 */
class EntityWithNullable
{
    /**
     * @var UuidInterface
     * @API\ApiProperty(identifier=true)
     */
    private $id;

    /**
     * @var string|null
     */
    private $nullableString;

    /**
     * @var Person|null
     */
    private $nullableEntity;

    /**
     * @var Person|null
     * @API\ApiProperty(readableLink=true, writableLink=true)
     */
    private $nullableEmbeddedEntity;

    /**
     * @var \DateTimeInterface|null
     */
    private $nullableDate;

    /**
     * EntityWithNullable constructor.
     *
     * @throws \Exception
     */
    public function __construct(?string $nullableString, ?Person $nullableEntity, ?Person $nullableEmbeddedEntity, ?\DateTimeInterface $nullableDate)
    {
        $this->id = Uuid::uuid4();
        $this->nullableString = $nullableString;
        $this->nullableEntity = $nullableEntity;
        $this->nullableEmbeddedEntity = $nullableEmbeddedEntity;
        $this->nullableDate = $nullableDate;
    }

    /**
     * Get id.
     */
    public function getId(): UuidInterface
    {
        return $this->id;
    }

    /**
     * Get nullableString.
     */
    public function getNullableString(): ?string
    {
        return $this->nullableString;
    }

    /**
     * Set nullableString.
     */
    public function setNullableString(?string $nullableString): void
    {
        $this->nullableString = $nullableString;
    }

    /**
     * Get nullableEntity.
     */
    public function getNullableEntity(): ?Person
    {
        return $this->nullableEntity;
    }

    /**
     * Set nullableEntity.
     */
    public function setNullableEntity(?Person $nullableEntity): void
    {
        $this->nullableEntity = $nullableEntity;
    }

    /**
     * Get nullableEmbeddedEntity.
     */
    public function getNullableEmbeddedEntity(): ?Person
    {
        return $this->nullableEmbeddedEntity;
    }

    /**
     * Set nullableEmbeddedEntity.
     */
    public function setNullableEmbeddedEntity(?Person $nullableEmbeddedEntity): void
    {
        $this->nullableEmbeddedEntity = $nullableEmbeddedEntity;
    }

    /**
     * Get nullableDate.
     */
    public function getNullableDate(): ?\DateTimeInterface
    {
        return $this->nullableDate;
    }

    /**
     * Set nullableDate.
     */
    public function setNullableDate(?\DateTimeInterface $nullableDate): void
    {
        $this->nullableDate = $nullableDate;
    }
}
