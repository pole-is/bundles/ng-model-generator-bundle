<?php
declare(strict_types=1);
/*
 * This file is part of "irstea/ng-model-generator-bundle".
 *
 * "irstea/ng-model-generator-bundle" generates Typescript interfaces for Angular using api-platform metadata.
 * Copyright (C) 2018-2021 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\NgModelGeneratorBundle\Tests\Fixtures\Entity;

use ApiPlatform\Core\Annotation as API;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use Ramsey\Uuid\UuidInterface;

/**
 * Class Project.
 *
 * @ORM\Entity()
 * @API\ApiResource()
 * @API\ApiFilter(SearchFilter::class, properties={"id":"exacté", "name":"partial", "acronym":"partial"})
 * @API\ApiFilter(OrderFilter::class, properties={"startDate"})
 */
class Project
{
    /**
     * @var UuidInterface
     * @ORM\Id()
     * @ORM\Column()
     */
    private $id;

    /**
     * @ORM\Column()
     *
     * @var string
     */
    private $name;

    /**
     * @ORM\Column()
     *
     * @var string
     */
    private $acronym;

    /**
     * @ORM\Column()
     *
     * @var \DateTimeInterface
     */
    private $startDate;

    /**
     * @ORM\Column(nullable=true)
     *
     * @var \DateTimeInterface|null
     */
    private $endDate;

    /**
     * @ORM\ManyToMany(targetEntity=Person::class)
     *
     * @var Person[]|Collection
     */
    private $participants;

    /**
     * Project constructor.
     *
     * @throws \Exception
     */
    public function __construct(
        string $name,
        string $acronym,
        \DateTimeInterface $startDate,
        array $participants,
        \DateTimeInterface $endDate = null
    ) {
        $this->id = Uuid::uuid4();
        $this->name = $name;
        $this->acronym = $acronym;
        $this->startDate = $startDate;
        $this->participants = $participants;
        $this->endDate = $endDate;
    }

    /**
     * Get id.
     */
    public function getId(): UuidInterface
    {
        return $this->id;
    }

    /**
     * Get name.
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Set name.
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * Get acronym.
     */
    public function getAcronym(): string
    {
        return $this->acronym;
    }

    /**
     * Set acronym.
     */
    public function setAcronym(string $acronym): void
    {
        $this->acronym = $acronym;
    }

    /**
     * Get startDate.
     */
    public function getStartDate(): \DateTimeInterface
    {
        return $this->startDate;
    }

    /**
     * Set startDate.
     */
    public function setStartDate(\DateTimeInterface $startDate): void
    {
        $this->startDate = $startDate;
    }

    /**
     * Get endDate.
     */
    public function getEndDate(): ?\DateTimeInterface
    {
        return $this->endDate;
    }

    /**
     * Set endDate.
     */
    public function setEndDate(?\DateTimeInterface $endDate): void
    {
        $this->endDate = $endDate;
    }

    /**
     * Get participants.
     */
    public function getParticipants(): array
    {
        return $this->participants;
    }

    /**
     * Set participants.
     */
    public function setParticipants(array $participants): void
    {
        $this->participants = $participants;
    }
}
