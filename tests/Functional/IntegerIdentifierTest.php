<?php
declare(strict_types=1);
/*
 * This file is part of "irstea/ng-model-generator-bundle".
 *
 * "irstea/ng-model-generator-bundle" generates Typescript interfaces for Angular using api-platform metadata.
 * Copyright (C) 2018-2021 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\NgModelGeneratorBundle\Tests\Functional;

/**
 * Class IntegerIdentifierTest.
 */
final class IntegerIdentifierTest extends GenerationTestCase
{
    /**
     * @param string $code
     *
     * @dataProvider listExpectedSnippets
     */
    public function testCheckCodeSnippet(string $snippet): void
    {
        self::assertCodeContains($snippet, self::generateTypescript());
    }

    public function listExpectedSnippets(): array
    {
        return [
            'EntityWithIntegerIdentifier' => [
                <<<'TYPESCRIPT'
export interface EntityWithIntegerIdentifier {
    readonly '@id': IRI<EntityWithIntegerIdentifier>;
    id: number;
    readonly '@type': 'EntityWithIntegerIdentifier';
}
TYPESCRIPT
                ,
            ],
            'Metadata'     => [
                <<<'TYPESCRIPT'
new ResourceMetadata<EntityWithIntegerIdentifier, 'EntityWithIntegerIdentifier', number>(
    'EntityWithIntegerIdentifier',
    new IRIMetadata(
        /^\/[^\/]+\/[^\/]+\/[^\/]*$/u,
        /^\/[^\/]+\/[^\/]+\/([^\/]*)$/u,
        (id: number) => `/api/entity_with_integer_identifiers/${id}`
    ),
    [ '@id', 'id', '@type', ],
),
TYPESCRIPT
                ,
            ],
        ];
    }
}
