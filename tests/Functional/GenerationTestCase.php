<?php
declare(strict_types=1);
/*
 * This file is part of "irstea/ng-model-generator-bundle".
 *
 * "irstea/ng-model-generator-bundle" generates Typescript interfaces for Angular using api-platform metadata.
 * Copyright (C) 2018-2021 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\NgModelGeneratorBundle\Tests\Functional;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class GenerationTestCase.
 */
abstract class GenerationTestCase extends KernelTestCase
{
    /** @var string|null */
    private static $generatedTypescript;

    /**
     * @throws \Exception
     */
    public static function generateTypescript(): string
    {
        if (self::$generatedTypescript !== null) {
            return self::$generatedTypescript;
        }

        $kernel = self::createKernel();
        $app = new Application($kernel);
        $app->setAutoExit(false);

        $input = new ArrayInput(['command' => 'ng-model:generate']);
        $input->setInteractive(false);

        $buffer = new BufferedOutput(OutputInterface::VERBOSITY_DEBUG);

        $result = $app->run($input, $buffer);
        $output = $buffer->fetch();

        self::assertEquals(0, $result, 'ng-model:generate failed: ' . $output);
        self::assertNotEmpty($output, 'ng-model:generate produced nothing');

        return self::$generatedTypescript = $output;
    }

    /**
     * Vérifie que $code contient $needle en ignorant les différences de whitespaces.
     */
    protected static function assertCodeContains(
        string $needle,
        string $code,
        string $message = ''
    ): void {
        $needleRegexp = '/' . preg_replace('/\s+/', '\\s+', preg_quote($needle, '/')) . '/';
        self::assertRegExp($needleRegexp, $code, $message);
    }
}
