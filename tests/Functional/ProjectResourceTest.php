<?php
declare(strict_types=1);
/*
 * This file is part of "irstea/ng-model-generator-bundle".
 *
 * "irstea/ng-model-generator-bundle" generates Typescript interfaces for Angular using api-platform metadata.
 * Copyright (C) 2018-2021 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\NgModelGeneratorBundle\Tests\Functional;

/**
 * Class ProjectResourceTest.
 */
final class ProjectResourceTest extends GenerationTestCase
{
    /**
     * @param string $code
     *
     * @dataProvider listExpectedSnippets
     */
    public function testCheckCodeSnippet(string $snippet): void
    {
        self::assertCodeContains($snippet, self::generateTypescript());
    }

    /**
     * Vérifie que $code contient $needle en ignorant les différences de whitespaces.
     */
    protected static function assertCodeContains(
        string $needle,
        string $code,
        string $message = ''
    ): void {
        $needleRegexp = '/' . preg_replace('/\s+/', '\\s+', preg_quote($needle, '/')) . '/';
        self::assertRegExp($needleRegexp, $code, $message);
    }

    /**
     * Fournit la liste des snippets à vérifier.
     */
    public function listExpectedSnippets(): array
    {
        return [
            'ProjectType'     => [
                <<<'TYPESCRIPT'
export interface Project {
  readonly '@id': IRI<Project>;
  readonly id: string;
  readonly '@type': 'Project';
  acronym: string;
  name: string;
  participants: Array<IRI<Person>>;
  startDate: string;
  endDate?: string;
}
TYPESCRIPT
                ,
            ],

            'PutProjectType'  => [
                <<<'TYPESCRIPT'
export interface PutProject {
  readonly '@id'?: IRI<Project>;
  readonly '@type'?: 'Project';
  acronym?: string;
  endDate?: string;
  name?: string;
  participants?: Array<IRI<Person>>;
  startDate?: string;
}
TYPESCRIPT
                ,
            ],

            'PostProjectType' => [
                <<<'TYPESCRIPT'
export interface PostProject {
  acronym: string;
  name: string;
  participants: Array<IRI<Person>>;
  startDate: string;
  endDate?: string;
}
TYPESCRIPT
                ,
            ],

            'GetAllFilter'    => [
                <<<'TYPESCRIPT'
export interface GetAllProjectFilters {
  acronym?: string;
  id?: string;
  name?: string;
  orderStartDate?: Ordering;
  page?: number;
  pageSize?: number;
}
TYPESCRIPT
                ,
            ],

            'Repository'             => ['export class ProjectRepository extends AbstractRepository<Project, \'Project\', string>'],
            'RepositoryGetMethod'    => ['public get(iri: IRI<Project>, options: RequestOptions = {}): Observable<Project> {'],
            'RepositoryPutMethod'    => ['public put(iri: IRI<Project>, body: PutProject, options: RequestOptions = {}): Observable<Project> {'],
            'RepositoryDeleteMethod' => ['public delete(iri: IRI<Project>, options: RequestOptions = {}): Observable<HttpResponseBase> {'],
            'RepositoryGetAllMethod' => ['public getAll(filters?: GetAllProjectFilters, options: RequestOptions = {}): Observable<Collection<Project>> {'],
            'RepositoryPostMethod'   => ['public post(body: PostProject, options: RequestOptions = {}): Observable<Project> {'],

            'Metadata' => [
                <<<'TYPESCRIPT'
Project: () => new ResourceMetadata<Project, 'Project', string>(
    'Project',
    new IRIMetadata(
        /^\/[^\/]+\/[^\/]+\/[^\/]*$/u,
        /^\/[^\/]+\/[^\/]+\/([^\/]*)$/u,
        (id: string) => `/api/projects/${id}`
    ),
    [ '@id', 'id', '@type', 'acronym', 'name', 'participants', 'startDate', ],
),
TYPESCRIPT
                ,
            ],

            'Provider' => ['{ provide: ProjectRepository,'],

            'ApiMeta'  => [
                <<<'TYPESCRIPT'
Project: {
    resource: Project;
    repository: ProjectRepository;
    metadata: ResourceMetadata<Project, 'Project', string>;
    iriParameters: string;
};
TYPESCRIPT
    ,
            ],
        ];
    }
}
