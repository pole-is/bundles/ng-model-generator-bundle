<?php
declare(strict_types=1);
/*
 * This file is part of "irstea/ng-model-generator-bundle".
 *
 * "irstea/ng-model-generator-bundle" generates Typescript interfaces for Angular using api-platform metadata.
 * Copyright (C) 2018-2021 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\NgModelGeneratorBundle\Tests\Functional;

/**
 * Class RelationTest.
 */
final class RelationTest extends GenerationTestCase
{
    /**
     * @param string $code
     *
     * @dataProvider listExpectedSnippets
     */
    public function testCheckCodeSnippet(string $snippet): void
    {
        self::assertCodeContains($snippet, self::generateTypescript());
    }

    public function listExpectedSnippets(): array
    {
        return [
            'EntityWithRelations' => [
                <<<'TYPESCRIPT'
export interface EntityWithRelations {
  readonly '@id': IRI<EntityWithRelations>;
  id: number;
  readonly '@type': 'EntityWithRelations';
  defaultMany: Array<IRI<Person>>;
  defaultSingle: IRI<Person>;
  readableMany: Array<Person>;
  readableSingle: Person;
  rwMany: Array<Person>;
  rwSingle: Person;
  writableMany: Array<IRI<Person>>;
  writableSingle: IRI<Person>;
}
TYPESCRIPT
                ,
            ],
            'PostEntityWithRelations' => [
                <<<'TYPESCRIPT'
export interface PostEntityWithRelations {
  id: number;
  defaultMany: Array<IRI<Person>>;
  defaultSingle: IRI<Person>;
  readableMany: Array<IRI<Person>>;
  readableSingle: IRI<Person>;
  rwMany: Array<PostEntityWithRelationsPerson>;
  rwSingle: PostEntityWithRelationsPerson;
  writableMany: Array<PostEntityWithRelationsPerson>;
  writableSingle: PostEntityWithRelationsPerson;
}
TYPESCRIPT
                ,
            ],
            'PutEntityWithRelations' => [
                <<<'TYPESCRIPT'
export interface PutEntityWithRelations {
  readonly '@id'?: IRI<EntityWithRelations>;
  id?: number;
  readonly '@type'?: 'EntityWithRelations';
  defaultMany?: Array<IRI<Person>>;
  defaultSingle?: IRI<Person>;
  readableMany?: Array<IRI<Person>>;
  readableSingle?: IRI<Person>;
  rwMany?: Array<PutEntityWithRelationsPerson>;
  rwSingle?: PutEntityWithRelationsPerson;
  writableMany?: Array<PutEntityWithRelationsPerson>;
  writableSingle?: PutEntityWithRelationsPerson;
}
TYPESCRIPT
                ,
            ],
        ];
    }
}
