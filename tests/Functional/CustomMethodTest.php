<?php
declare(strict_types=1);
/*
 * This file is part of "irstea/ng-model-generator-bundle".
 *
 * "irstea/ng-model-generator-bundle" generates Typescript interfaces for Angular using api-platform metadata.
 * Copyright (C) 2018-2021 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\NgModelGeneratorBundle\Tests\Functional;

/**
 * Class CustomMethodTest.
 */
final class CustomMethodTest extends GenerationTestCase
{
    /**
     * @param string $code
     *
     * @dataProvider listExpectedSnippets
     */
    public function testCheckCodeSnippet(string $snippet): void
    {
        self::assertCodeContains($snippet, self::generateTypescript());
    }

    public function listExpectedSnippets(): array
    {
        return [
            'CustomItemMethod'       => ['public custom(iri: IRI<EntityWithCustomMethods>, body: CustomEntityWithCustomMethods, options: RequestOptions = {}): Observable<EntityWithCustomMethods> {'],
            'CustomCollectionMethod' => ['public customs(body: Array<CustomEntityWithCustomMethods>, options: RequestOptions = {}): Observable<Collection<EntityWithCustomMethods>> {'],
        ];
    }
}
