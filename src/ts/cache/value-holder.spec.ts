import { forkJoin } from 'rxjs';
import { IRI, IRI_PROPERTY, Resource } from '../types';
import { ValueHolder } from './value-holder';
import { IRIMismatchError, MissingIRIError } from '../types/errors';
import { forkJoinArray } from 'rxjs-etc';
import { marbles } from 'rxjs-marbles';
import { cases } from 'rxjs-marbles/jest';

interface MyResource extends Resource {
    readonly '@id': IRI<MyResource>;
    readonly '@type': 'MyResource';
    value: string;
    value2?: string;
}

function iri(x: string): IRI<MyResource> {
    return x as any;
}

const MY_IRI = iri('/bla/a');
const OTHER_IRI = iri('/bla/B');
const VALUES: { [name: string]: MyResource } = {
    a: { '@id': MY_IRI, '@type': 'MyResource', value: 'foo' },
    b: { '@id': MY_IRI, '@type': 'MyResource', value: 'bar' },
    c: {
        '@id': MY_IRI,
        '@type': 'MyResource',
        value: 'bar',
        value2: 'quz',
    },
    d: { '@id': OTHER_IRI, '@type': 'MyResource', value: 'zig' },
};

describe('ValueHolder', () => {
    let holder: ValueHolder<any>;

    beforeEach(() => {
        holder = new ValueHolder<MyResource>(iri('/bla/a'));
    });

    it('should be created', () => {
        expect(holder).toBeTruthy();
    });

    describe('.set()', () => {
        cases(
            'foo',
            ({ expect }, { value, error, SET_M }) =>
                // eslint-disable-next-line jest/no-standalone-expect
                expect(holder.set(value)).toBeObservable(SET_M, VALUES, error),
            [
                {
                    name: 'should provide the value',
                    value: VALUES.a,
                    SET_M: '(a|)',
                },
                {
                    name: `should refuse value without ${IRI_PROPERTY}`,
                    value: {},
                    error: new MissingIRIError(),
                    SET_M: '#',
                },
                {
                    name: 'should refuse value with different @id',
                    value: { '@id': iri('bar') },
                    error: new IRIMismatchError(
                        MY_IRI.toString(),
                        iri('bar').toString(),
                    ),
                    SET_M: '#',
                },
            ],
        );

        it('should always points to the same instance', () => {
            forkJoin(holder.set(VALUES.a), holder.set(VALUES.b)).subscribe(
                ([a, b]: MyResource[]) => {
                    expect(a).toBe(b);
                },
            );
        });
    });

    describe('.update()', () => {
        it('should provide the value from the server', () =>
            marbles(({ cold, expect }) => {
                const REQ_M = '---a|';
                const UPD_M = '--(a|) ';
                const request$ = cold(REQ_M, VALUES);

                const response$ = holder.update(request$);

                expect(response$).toBeObservable(UPD_M, VALUES);
            }));

        it('should cancel pending requests', () =>
            marbles(({ cold, expect }) => {
                const LOCAL_VALUES: { [key: string]: any } = {
                    a: VALUES.a,
                    b: VALUES.b,
                    j: [VALUES.b, VALUES.b],
                };
                const REQ1_M = '---a|';
                const REQ2_M = 'b|   ';

                const UPDA_M = '(j|) ';
                const REQ1_S = '(^!) ';
                const REQ2_S = '(^!) ';

                const request1$ = cold(REQ1_M, LOCAL_VALUES);
                const request2$ = cold(REQ2_M, LOCAL_VALUES);

                expect(
                    forkJoinArray([
                        //
                        holder.update(request1$),
                        holder.update(request2$),
                    ]),
                ).toBeObservable(UPDA_M, LOCAL_VALUES);
                expect(request1$).toHaveSubscriptions(REQ1_S);
                expect(request2$).toHaveSubscriptions(REQ2_S);
            }));

        it('should propagate errors', () =>
            marbles(({ cold, expect }) => {
                const REQ_M = '#';
                const UPD_M = '#';

                const request$ = cold(REQ_M, VALUES);
                expect(holder.update(request$)).toBeObservable(UPD_M, VALUES);
            }));

        it('should restart on errors', () =>
            marbles(({ cold, expect }) => {
                const REQ1_M = '#';
                const UPD1_M = '#';

                const obs$ = holder.update(cold(REQ1_M, VALUES));
                expect(obs$).toBeObservable(UPD1_M, VALUES);

                const REQ2_M = '-a|';
                const UPD2_M = '-a|';

                const obs2$ = holder.update(cold(REQ2_M, VALUES));
                expect(obs2$).toBeObservable(UPD2_M, VALUES);
            }));
    });

    describe('.listen()', () => {
        cases(
            '',
            ({ cold, expect }, { REQUEST_M, LISTEN_M, initial }) => {
                if (initial) {
                    holder.set(initial);
                }
                const query$ = cold(REQUEST_M, VALUES);

                const response$ = holder.listen(() => query$);

                // eslint-disable-next-line jest/no-standalone-expect
                expect(response$).toBeObservable(LISTEN_M, VALUES);
            },
            [
                {
                    name: 'should provide the value',
                    initial: VALUES.a,
                    REQUEST_M: /**/ '    ',
                    LISTEN_M: /***/ '(a|)',
                },
                {
                    name: 'should cache the value',
                    initial: VALUES.a,
                    REQUEST_M: /**/ 'b|  ',
                    LISTEN_M: /***/ '(a|)',
                },
                {
                    name: 'should propagate errors',
                    REQUEST_M: /**/ '#',
                    LISTEN_M: /***/ '#',
                },
            ],
        );
    });

    it('.invalidate() should cause the value to be requested again', () =>
        marbles((m) => {
            const REQUEST_M = /**/ '(a|)';
            const LISTEN_M = /***/ '(a|)';
            const requestFactory = jest.fn(() => m.cold(REQUEST_M, VALUES));

            return holder
                .set(VALUES.a)
                .toPromise()
                .then(() => holder.invalidate())
                .then(() =>
                    m
                        .expect(holder.listen(requestFactory))
                        .toBeObservable(LISTEN_M, VALUES),
                )
                .then(() => expect(requestFactory).toHaveBeenCalled());
        }));
});
