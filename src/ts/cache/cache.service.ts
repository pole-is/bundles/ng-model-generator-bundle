import { HttpResponseBase } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, switchMap, tap } from 'rxjs/operators';
import { forkJoinArray } from 'rxjs-etc';

import { ValueHolder } from './value-holder';

import {
    Collection,
    COLLECTION_MEMBERS,
    getCollectionMembers,
    IRI,
    IRI_PROPERTY,
    Resource,
} from '../types';
import { AbstractResourceCache } from '../api';

/**
 * Implémentation d'un cache de resource.
 *
 * Cette implémentation met en place une queue de requête ainsi qu'un observable pour chaque ressource.
 *
 * La queue de requête permet de mettre à jour une ressource en cache. switchMap est utilisé pour prendre en compte
 * les valeurs des dernières requêtes.
 *
 * L'Observable s'assure de retourner toujours la même référence d'objet tout au long de la vie
 * de la ressource dans le cache, il permet aussi de faire suivre tout mise à jour à d'eventuels subscribers.
 */
export class ResourceCache extends AbstractResourceCache {
    private readonly holders = new Map<IRI<any>, ValueHolder<any>>();

    /**
     * Retourne la ressource identifiée par l'IRI donné.
     *
     * Effectue une requête si on le connait pas.
     */
    public get<R extends Resource>(
        iri: IRI<R>,
        requestFactory: () => Observable<R>,
    ): Observable<R> {
        return this.getHolder(iri).listen(requestFactory);
    }

    /**
     * Envoie une requête de mise à jour puis met à jour le cache avec la réponse du serveur.
     */
    public put<R extends Resource>(
        iri: IRI<R>,
        query$: Observable<R>,
    ): Observable<R> {
        return this.getHolder(iri).update(query$);
    }

    /**
     * Envoie une requête de création puis met à jour le chache avec la réponse.
     */
    public post<R extends Resource>(query$: Observable<R>): Observable<R> {
        return query$.pipe(switchMap((item) => this.received(item)));
    }

    /**
     * Supprime une ressource sur le serveur puis en cache.
     */
    public delete<R extends Resource>(
        iri: IRI<R>,
        query$: Observable<HttpResponseBase>,
    ): Observable<HttpResponseBase> {
        return query$.pipe(
            tap(() => {
                const holder = this.holders.get(iri);
                if (holder) {
                    this.holders.delete(iri);
                    holder.delete();
                }
                this.holders.delete(iri);
            }),
        );
    }

    /**
     * Fait une requête pour plusieurs ressources puis les mets en cache.
     */
    public getAll<R extends Resource>(
        query$: Observable<Collection<R>>,
    ): Observable<Collection<R>> {
        return query$.pipe(
            switchMap((coll: Collection<R>) => {
                const members = getCollectionMembers(coll);
                const memberObservables$ = members.map((item) =>
                    this.received(item),
                );
                return forkJoinArray(memberObservables$).pipe(
                    map((items) =>
                        Object.assign({} as Collection<R>, coll, {
                            [COLLECTION_MEMBERS]: items,
                        }),
                    ),
                );
            }),
        );
    }

    /**
     * Invalide la valeur d'une IRI pour forcer une mise-à-jour.
     */
    public invalidate<R extends Resource>(iri: IRI<R>): void {
        const holder = this.holders.get(iri);
        if (holder) {
            holder.invalidate();
        }
    }

    /**
     * Retourne le ValueHolder d'une IRI, ou le crée si nécessaire.
     */
    private getHolder<R extends Resource>(iri: IRI<R>): ValueHolder<R> {
        let holder = this.holders.get(iri) as ValueHolder<R> | undefined;
        if (!holder) {
            holder = new ValueHolder<R>(iri);
            this.holders.set(iri, holder);
        }
        return holder;
    }

    /**
     * Retourne le ValueHolder d'une IRI, ou le crée si nécessaire.
     */
    private received<R extends Resource>(item: R): Observable<R> {
        return this.getHolder(item[IRI_PROPERTY]).set(item);
    }
}
