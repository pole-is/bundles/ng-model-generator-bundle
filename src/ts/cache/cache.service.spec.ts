import { HttpHeaderResponse } from '@angular/common/http';
import { inject, TestBed } from '@angular/core/testing';
import * as _ from 'lodash';
import { from, Observable } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';
import { marbles } from 'rxjs-marbles';

import { ResourceCache } from './cache.service';

import { Collection, IRI, Resource } from '../types';

interface MyResource extends Resource {
    readonly '@id': IRI<MyResource>;
    readonly '@type': 'MyResource';
    value: string;
    value2?: string;
}

function iri(x: string): IRI<MyResource> {
    return x as any;
}

const MY_IRI = iri('/bla/a');
const OTHER_IRI = iri('/bla/B');
const VALUES: { [name: string]: MyResource } = {
    a: { '@id': MY_IRI, '@type': 'MyResource', value: 'foo' },
    b: { '@id': MY_IRI, '@type': 'MyResource', value: 'bar' },
    c: {
        '@id': MY_IRI,
        '@type': 'MyResource',
        value: 'bar',
        value2: 'quz',
    },
    d: { '@id': OTHER_IRI, '@type': 'MyResource', value: 'zig' },
};

describe('ResourceCache', () => {
    beforeEach(() =>
        TestBed.configureTestingModule({
            providers: [ResourceCache],
        }),
    );

    it('should be created', inject(
        [ResourceCache],
        (service: ResourceCache) => {
            expect(service).toBeDefined();
        },
    ));

    describe('.get()', () => {
        it('should provide the value', inject(
            [ResourceCache],
            (service: ResourceCache) =>
                marbles(({ cold, expect }) => {
                    const getQuery$ = cold('a|', VALUES);

                    const response$ = service.get(MY_IRI, () => getQuery$);

                    expect(response$).toBeObservable('(a|)', VALUES);
                }),
        ));

        // it('should cache the value', inject([ResourceCache], (service: ResourceCache) =>
        //     marbles(({ cold, expect }) => {
        //         const getQuery$ = cold('a|', VALUES);
        //         const getQuery2$ = cold('b|', VALUES);
        //         // tslint:disable-next-line:rxjs-finnish
        //         const queries$ = cold('ab|', {
        //             a: getQuery$,
        //             b: getQuery2$,
        //         });
        //
        //         const response$ = queries$.pipe(mergeMap((query$) => service.get(MY_IRI, () => query$)));
        //
        //         expect(response$).toBeObservable('aa|', VALUES);
        //     }),
        // ));

        it('should propagate errors', inject(
            [ResourceCache],
            (service: ResourceCache) =>
                marbles(({ cold, expect }) => {
                    const getQuery$ = cold('#');

                    const response$ = service.get(MY_IRI, () => getQuery$);

                    expect(response$).toBeObservable('#');
                }),
        ));
    });

    describe('.put()', () => {
        it('should provide the value', inject(
            [ResourceCache],
            (service: ResourceCache) =>
                marbles(({ cold, expect }) => {
                    const putRequest$ = cold('a|', VALUES);

                    const response$ = service.put(MY_IRI, putRequest$);

                    expect(response$).toBeObservable('a|', VALUES);
                }),
        ));

        it('should not cache the value', inject(
            [ResourceCache],
            (service: ResourceCache) =>
                marbles(({ cold, expect }) => {
                    const putRequest$ = cold('a|', VALUES);
                    const putRequest2$ = cold('b|', VALUES);
                    // tslint:disable-next-line:rxjs-finnish
                    const requests$ = cold('ab|', {
                        a: putRequest$,
                        b: putRequest2$,
                    });

                    const response$ = requests$.pipe(
                        mergeMap((request$: Observable<MyResource>) =>
                            service.put(MY_IRI, request$),
                        ),
                        map((x) => _.clone(x)),
                    );

                    expect(response$).toBeObservable('ab|', VALUES);
                }),
        ));

        it('should propagate errors', inject(
            [ResourceCache],
            (service: ResourceCache) =>
                marbles(({ cold, expect }) => {
                    const putRequest$ = cold('#');

                    const response$ = service.put(MY_IRI, putRequest$);

                    expect(response$).toBeObservable('#');
                }),
        ));
    });

    describe('.post()', () => {
        it('should provide the value', inject(
            [ResourceCache],
            (service: ResourceCache) =>
                marbles(({ cold, expect }) => {
                    const postRequest$ = cold('a|', VALUES);

                    const response$ = service.post(postRequest$);

                    expect(response$).toBeObservable('a|', VALUES);
                }),
        ));

        it('should propagate errors', inject(
            [ResourceCache],
            (service: ResourceCache) =>
                marbles(({ cold, expect }) => {
                    const postRequest$ = cold('#', VALUES);

                    const response$ = service.post(postRequest$);

                    expect(response$).toBeObservable('#', VALUES);
                }),
        ));
    });

    describe('.delete()', () => {
        it('should clear the cache on successful fetch', inject(
            [ResourceCache],
            (service: ResourceCache) =>
                marbles(({ cold, expect }) => {
                    const response = new HttpHeaderResponse({ status: 200 });
                    const values = { r: response };

                    const getResponse$ = service.get(MY_IRI, () =>
                        cold('a|', VALUES),
                    );
                    const deleteResponse$ = service.delete(
                        MY_IRI,
                        cold('r|', values),
                    );
                    const secondGetResponse$ = service.get(MY_IRI, () =>
                        cold('b|'),
                    );

                    const all$ = from([
                        getResponse$,
                        deleteResponse$,
                        secondGetResponse$,
                    ]).pipe(mergeMap((x) => x));

                    expect(all$).toBeObservable('(b|)', VALUES);
                }),
        ));

        it('should propagate errors', inject(
            [ResourceCache],
            (service: ResourceCache) =>
                marbles(({ cold, expect }) => {
                    const deleteRequest$ = cold('#');

                    const response$ = service.delete(MY_IRI, deleteRequest$);

                    expect(response$).toBeObservable('#');
                }),
        ));
        //
        // it('should not clear the cache on error', inject([ResourceCache], (service: ResourceCache) => {
        //     const error = new HttpHeaderResponse({ status: 500 });
        //     const values = { a: VALUES.a, b: VALUES.b, e: error };
        //     scheduler
        //         .withValues(values)
        //         .withError(error)
        //         .run(async ({ cold, expect }) => {
        //             await service.get(MY_IRI, () => cold('a|')).toPromise();
        //             await service
        //                 .delete(MY_IRI, cold('#'))
        //                 .pipe(catchError((e) => e))
        //                 .toPromise();
        //
        //             return expect(service.get(MY_IRI, () => cold('b|'))).toBeObservable('(a|)');
        //         });
        // }));
    });

    describe('.getAll()', () => {
        // it('should provide the returned value', inject([ResourceCache], (service: ResourceCache) => {
        //     const values = {
        //         a: {
        //             'hydra:member': [VALUES.a, VALUES.d],
        //             'hydra:totalItems': 2,
        //         },
        //     };
        //     scheduler.withValues(values).run(({ cold, expect }) => {
        //         const getAllRequest$ = cold('a|');
        //         expect(service.getAll(getAllRequest$)).toBeObservable('a|');
        //     });
        // }));
        //
        // it('should nicely handle empty collections', inject([ResourceCache], (service: ResourceCache) => {
        //     const values = {
        //         a: { 'hydra:member': [] as any, 'hydra:totalItems': 0 },
        //     };
        //     scheduler.withValues(values).run(({ cold, expect }) => {
        //         const getAllRequest$ = cold('a|');
        //         expect(service.getAll(getAllRequest$)).toBeObservable('a|');
        //     });
        // }));

        it('should propagate errors', inject(
            [ResourceCache],
            (service: ResourceCache) =>
                marbles(({ cold, expect }) => {
                    const getAllRequest$ = cold<Collection<MyResource>>('#');
                    expect(service.getAll(getAllRequest$)).toBeObservable('#');
                }),
        ));
        //
        // it('should populate the cache', inject([ResourceCache], (service: ResourceCache) => {
        //     const values = {
        //         a: VALUES.a,
        //         b: VALUES.d,
        //         h: {
        //             'hydra:member': [VALUES.a, VALUES.d],
        //             'hydra:totalItems': 2,
        //         },
        //     };
        //     scheduler.withValues(values).run(({ cold, expect }) => {
        //         const getAllRequest$ = cold('h|');
        //         const getRequest$ = cold('b|');
        //         const requests$ = cold<() => Observable<any>>('ab|', {
        //             a: () => service.getAll(getAllRequest$),
        //             b: () => service.get(MY_IRI, () => getRequest$),
        //         });
        //
        //         expect(requests$.pipe(mergeMap((sendRequest: () => Observable<any>) => sendRequest()))).toBeObservable(
        //             'ha|',
        //         );
        //     });
        // }));
    });
});
