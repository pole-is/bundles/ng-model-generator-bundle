import { Observable, of, race, Subject, throwError } from 'rxjs';
import * as _ from 'lodash';
import { switchMap, take } from 'rxjs/operators';

import {
    IRI,
    IRI_PROPERTY,
    Resource,
    IRIMismatchError,
    MissingIRIError,
} from '../types';

/**
 * ValueHolder gère les requêtes d'une seule ressource.
 *
 * @internal
 */
export class ValueHolder<R extends Resource> {
    private readonly value$ = new Subject<R>();

    private readonly value = {} as R;
    private version = 0;

    constructor(private readonly iri: IRI<R>) {}

    public set(value: R): Observable<R> {
        if (!(IRI_PROPERTY in value)) {
            return throwError(new MissingIRIError());
        }
        if (value[IRI_PROPERTY] !== this.iri) {
            return throwError(
                new IRIMismatchError(
                    this.iri.toString(),
                    value[IRI_PROPERTY].toString(),
                ),
            );
        }

        _.assign(this.value, value);
        _.chain(this.value)
            .keys()
            .difference(_.keys(value))
            .forEach((key) => delete this.value[key]);
        this.version++;
        this.value$.next(this.value);

        return of(this.value);
    }

    public listen(queryFactory: () => Observable<R>): Observable<R> {
        if (this.version > 0) {
            return of(this.value);
        }
        return this.update(queryFactory());
    }

    public update(request$: Observable<R>): Observable<R> {
        return race(
            this.value$.pipe(take(1)),
            request$.pipe(switchMap((item: R) => this.set(item))),
        );
    }

    public invalidate(): void {
        this.version = 0;
    }

    public delete(): void {
        this.value$.complete();
    }
}
