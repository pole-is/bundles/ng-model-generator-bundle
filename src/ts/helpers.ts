// Helper type-guard
// Checks that an object has a defined value for the given property.
export function hasProperty<
    T extends { [name: string]: any },
    K extends keyof T
>(obj: T, property: K): obj is T & { [X in K]-?: T[X] } {
    return (
        typeof obj === 'object' && obj !== null && obj[property] !== undefined
    );
}
