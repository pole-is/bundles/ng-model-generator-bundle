import { IRI, Resource } from './resource';

type IRIParameters = string | string[];

export interface Repository<
    R extends Resource,
    T extends string,
    P extends IRIParameters
> {
    readonly resourceType: T;
    readonly requiredProperties: ReadonlyArray<string>;
    readonly iriPattern: RegExp;

    isRequired(propertyName: string): boolean;

    generateIRI(parameters: P): IRI<R>;

    getIRIParameters(iri: IRI<R>): P;
}

export type ResourceType<X> = X extends Repository<infer R, any, any>
    ? R
    : never;

export type ResourceTypeString<X> = X extends Repository<any, infer T, any>
    ? T
    : never;

export type IRIParameterType<X> = X extends Repository<any, any, infer P>
    ? P
    : never;

export abstract class AbstractRepository<
    R extends Resource,
    T extends string,
    P extends IRIParameters
> implements Repository<R, T, P> {
    protected constructor(
        public readonly resourceType: T,
        public readonly requiredProperties: ReadonlyArray<string>,
        public readonly iriPattern: RegExp,
    ) {}

    public isRequired(propertyName: string): boolean {
        return this.requiredProperties.indexOf(propertyName) >= 0;
    }

    public abstract generateIRI(parameters: P): IRI<R>;

    public abstract getIRIParameters(iri: IRI<R>): P;
}
