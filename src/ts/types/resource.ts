/**
 * Nom de la propriété d'une resource contenant son IRI.
 */
export const IRI_PROPERTY = '@id';

/**
 * Nom de la propriété d'une resource contenant son type.
 */
export const TYPE_PROPERTY = '@type';

/**
 * IRI typé.
 *
 * Internationalized Resource Identifier - RFC 3987
 */
const IRI = Symbol('IRI');

/* Les IRI sont en fait des chaînes mais pour forcer un typage fort on les définit
 * comme un type "opaque". De cette façon, il est impossible de mélanger
 * IRI et chaînes, et le type générique R permet d'interdire les assignations entre
 * IRI de resources différentes.
 */
export interface IRI<R extends Resource> {
    readonly [IRI]?: R;
}

/**
 * Resource
 */
export interface Resource {
    readonly [IRI_PROPERTY]: IRI<any>;
    readonly [TYPE_PROPERTY]: string;
    [property: string]: any;
}
