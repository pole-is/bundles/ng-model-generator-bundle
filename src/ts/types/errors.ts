import { IRI_PROPERTY } from './index';

export class InvalidIRIError extends Error {}

export class APICacheError extends Error {}

export class MissingIRIError extends APICacheError {
    public constructor() {
        super(`resource must have an ${IRI_PROPERTY} property`);
    }
}

export class IRIMismatchError extends APICacheError {
    public constructor(expected: string, actual: string) {
        super(`${IRI_PROPERTY}s mismatch: ${actual} !== ${expected}`);
    }
}
