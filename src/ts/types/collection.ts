import { Resource } from './resource';

/**
 * Nom de la propriété contenant les resource d'une collection.
 */
export const COLLECTION_MEMBERS = 'hydra:member';

/**
 * Nom de la propriété contenant le nombre total d'objet d'une collection.
 */
export const COLLECTION_TOTAL_COUNT = 'hydra:totalItems';

/**
 * Collection représente une collection de respoucres JSON-LD pour un type T donné.
 */
export interface Collection<R extends Resource> {
    [COLLECTION_MEMBERS]: R[];
    [COLLECTION_TOTAL_COUNT]: number;
    [property: string]: any;
}

/**
 * Retourne les membres d'une collection.
 */
export function getCollectionMembers<R extends Resource>(
    collection: Collection<R>,
): R[] {
    return collection[COLLECTION_MEMBERS];
}

/**
 * Retourne le nombre total d'items
 * @param collection
 */
export function getCollectionTotalCount<R extends Resource>(
    collection: Collection<R>,
): number {
    return collection[COLLECTION_TOTAL_COUNT];
}
