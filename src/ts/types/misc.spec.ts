import { isDateTime } from './misc';

describe('isDateTime', () => {
    const itShouldRejectType = (value: unknown) =>
        it(`should reject ${JSON.stringify(value)}`, () =>
            expect(isDateTime(value)).toBeFalsy());

    itShouldRejectType(false);
    itShouldRejectType(null);
    itShouldRejectType(undefined);
    itShouldRejectType([]);
    itShouldRejectType({ value: 'foo' });

    const itShouldReject = (value: string) =>
        it(`should reject ${value}`, () =>
            expect(isDateTime(value)).toBeFalsy());

    itShouldReject('foobar');
    itShouldReject('2000-01-32');
    itShouldReject('2000-01-32T00:00');

    itShouldReject('2000-01-32T00:00:00Z');
    itShouldReject('2000-13-00T00:00:00Z');
    itShouldReject('2000-26-00T00:00:00Z');
    itShouldReject('2000-01-01T25:00:00Z');
    itShouldReject('2000-01-01T35:00:00Z');
    itShouldReject('2000-01-01T00:60:00Z');
    itShouldReject('2000-01-01T00:00:72Z');
    itShouldReject('2000-01-51T00:00:00Z');

    const itShouldAccept = (value: string) =>
        it(`should accept ${value}`, () =>
            expect(isDateTime(value)).toBeTruthy());

    for (
        let t = Date.parse('2000-01-01T00:00:00Z');
        t < Date.parse('2001-01-01T00:00:00Z');
        t += 86400000
    ) {
        const dateStr = new Date(t).toISOString();
        itShouldAccept(dateStr);
    }
});
