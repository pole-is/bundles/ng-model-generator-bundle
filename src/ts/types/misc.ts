/**
 * Full DateTime in ISO-8601 format.
 */
export type DateTime = string;

const DateTimeRegex = /^\d{4}-(?:0\d|1[012])-(?:3[01]|[012]\d)T(?:2[0-3]|[01]\d):[0-5]\d:[0-5]\d(?:\.\d+)?(?:[-+]\d{2}:\d{2}|Z)$/iu;

export function isDateTime(data: unknown): data is DateTime {
    return (
        typeof data === 'string' &&
        DateTimeRegex.test(data) &&
        !Number.isNaN(Date.parse(data))
    );
}

/**
 * Universally Unique Identifier - RFC 4122
 */
export type UUID = string;

const UUIDRegex = /^[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}$/iu;

/**
 * Teste si une donnée (chaîne) est formatée selon un UUID.
 */
export function isUUID(data: unknown): data is UUID {
    return typeof data === 'string' && UUIDRegex.test(data);
}
