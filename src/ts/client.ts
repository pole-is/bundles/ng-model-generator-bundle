import { Injectable } from '@angular/core';
import {
    HttpClient as NgHttpClient,
    HttpHeaders,
    HttpResponse,
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';

import { Collection, IRI, Resource } from './types';

export interface RequestOptions {
    headers?:
        | HttpHeaders
        | {
              [header: string]: string | string[];
          };
    params?: { [param: string]: string | string[] };
}

export interface BodyRequestOptions<B = any> extends RequestOptions {
    body?: B;
}

export abstract class Client {
    public abstract getItem<R extends Resource>(
        iri: IRI<R>,
        options: RequestOptions,
    ): Observable<HttpResponse<R>>;

    public abstract putItem<R extends Resource, B = R>(
        iri: IRI<R>,
        body: B,
        options: BodyRequestOptions<B>,
    ): Observable<HttpResponse<R>>;

    public abstract postItem<R extends Resource, B = R>(
        iri: IRI<R>,
        body: B,
        options: BodyRequestOptions<B>,
    ): Observable<HttpResponse<R>>;

    public abstract deleteItem<R extends Resource>(
        iri: IRI<R>,
        options: RequestOptions,
    ): Observable<HttpResponse<void>>;

    public abstract getCollection<R extends Resource>(
        iri: string,
        options: RequestOptions,
    ): Observable<HttpResponse<Collection<R>>>;

    public abstract purgeItem<R extends Resource>(iri: IRI<R>): void;

    public abstract purgeCollection(iri: string): void;

    public abstract request<T, B = any>(
        method: string,
        url: string,
        body?: B,
        options?: BodyRequestOptions<B>,
    ): Observable<HttpResponse<T>>;
}

@Injectable({
    providedIn: 'root',
    deps: [NgHttpClient],
    useFactory: (httpClient: NgHttpClient) => new HttpClient(httpClient),
})
export class HttpClient extends Client {
    public constructor(private readonly httpClient: NgHttpClient) {
        super();
    }

    public deleteItem<R extends Resource>(
        iri: IRI<R>,
        options: RequestOptions,
    ): Observable<HttpResponse<void>> {
        return this.request('DELETE', iri.toString(), undefined, options);
    }

    public getCollection<R extends Resource>(
        iri: string,
        options: RequestOptions,
    ): Observable<HttpResponse<Collection<R>>> {
        return this.request('GET', iri, undefined, options);
    }

    public getItem<R extends Resource>(
        iri: IRI<R>,
        options: RequestOptions,
    ): Observable<HttpResponse<R>> {
        return this.request('GET', iri.toString(), undefined, options);
    }

    public postItem<R extends Resource, B = R>(
        iri: IRI<R>,
        body: B,
        options: BodyRequestOptions<B>,
    ): Observable<HttpResponse<R>> {
        return this.request('POST', iri.toString(), body, options);
    }

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    public purgeCollection(iri: string): void {
        // NOOP
    }

    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    public purgeItem<R extends Resource>(iri: IRI<R>): void {
        // NOOP
    }

    public putItem<R extends Resource, B = R>(
        iri: IRI<R>,
        body: B,
        options: BodyRequestOptions<B>,
    ): Observable<HttpResponse<R>> {
        return this.request('PUT', iri.toString(), body, options);
    }

    public request<T, B = any>(
        method: string,
        uri: string,
        body?: B,
        options: BodyRequestOptions<B> = {},
    ): Observable<HttpResponse<T>> {
        return this.httpClient
            .request<T>(method, uri, {
                ...options,
                body: options.body || body,
                reportProgress: false,
                observe: 'response',
                responseType: 'json',
            })
            .pipe(take(1));
    }
}
