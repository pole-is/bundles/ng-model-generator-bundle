<?php
declare(strict_types=1);
/*
 * This file is part of "irstea/ng-model-generator-bundle".
 *
 * "irstea/ng-model-generator-bundle" generates Typescript interfaces for Angular using api-platform metadata.
 * Copyright (C) 2018-2021 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\NgModelGeneratorBundle;

use Irstea\NgModelGeneratorBundle\Exceptions\TypeAlreadyExistsException;
use Irstea\NgModelGeneratorBundle\Exceptions\TypeNotFoundException;
use Irstea\NgModelGeneratorBundle\Models\Types\Deferred;
use Irstea\NgModelGeneratorBundle\Models\Types\Reference;
use Irstea\NgModelGeneratorBundle\Models\Types\Type;

/**
 * Class TypeFactory.
 */
final class TypeFactory implements TypeFactoryInterface, \IteratorAggregate
{
    /** @var Type[] */
    private $types = [];

    /**
     * {@inheritdoc}
     */
    public function getIterator()
    {
        yield from $this->types;
    }

    public function has(string $name): bool
    {
        return isset($this->types[$name]);
    }

    public function get(string $name): Type
    {
        // API-platform semble avoir un problème avec les types utilisés dans un Trait.
        if (strpos($name, '\UuidInterface') !== false) {
            $name = 'Ramsey\Uuid\UuidInterface';
        }

        if (!isset($this->types[$name])) {
            throw new TypeNotFoundException("type not defined: ${name}");
        }

        return $this->types[$name];
    }

    /**
     * {@inheritdoc}
     */
    public function add(string $name, Type $type): void
    {
        if (isset($this->types[$name])) {
            throw new TypeAlreadyExistsException("type already exists: ${name}");
        }

        $this->types[$name] = $type;
    }

    /**
     * {@inheritdoc}
     */
    public function defer(string $name): Deferred
    {
        if (isset($this->types[$name]) && $this->types[$name] instanceof Deferred) {
            /* @noinspection PhpIncompatibleReturnTypeInspection */
            return $this->types[$name];
        }
        $ref = new Reference($name);
        $this->add($name, $ref);

        return $ref;
    }
}
