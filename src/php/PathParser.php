<?php
declare(strict_types=1);
/*
 * This file is part of "irstea/ng-model-generator-bundle".
 *
 * "irstea/ng-model-generator-bundle" generates Typescript interfaces for Angular using api-platform metadata.
 * Copyright (C) 2018-2021 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\NgModelGeneratorBundle;

use Assert\Assertion;
use Irstea\NgModelGeneratorBundle\Models\Types\BuiltinType;
use Irstea\NgModelGeneratorBundle\Models\Types\Objects\Property;
use Irstea\NgModelGeneratorBundle\Models\Types\Operations\AnyPathPart;
use Irstea\NgModelGeneratorBundle\Models\Types\Operations\Parameter;
use Irstea\NgModelGeneratorBundle\Models\Types\Operations\ParameterPathPart;
use Irstea\NgModelGeneratorBundle\Models\Types\Operations\Path;

/**
 * Class PathParser.
 */
final class PathParser implements PathParserInterface
{
    /** @var Property[] */
    private $properties;

    /**
     * PathParser constructor.
     *
     * @param Property[] $properties
     */
    public function __construct(array $properties)
    {
        $this->properties = $properties;
    }

    /**
     * {@inheritdoc}
     */
    public function parse(string $path, array $requirements): Path
    {
        $stringParts = preg_split('/\{(\w+)\}/', $path, -1, \PREG_SPLIT_NO_EMPTY | \PREG_SPLIT_DELIM_CAPTURE);
        Assertion::isArray($stringParts);

        $parts = [];

        $num = \count($stringParts);
        for ($i = 0; $i < $num; ++$i) {
            if ($stringParts[$i]) {
                $parts[] = new AnyPathPart($stringParts[$i]);
            }
            ++$i;

            if ($i >= $num) {
                break;
            }

            $name = $stringParts[$i];

            if (isset($this->properties[$name])) {
                $property = $this->properties[$name];
                $type = $property->getType();
            } else {
                $type = BuiltinType::get('string');
            }

            $parameter = new Parameter($name, $type);
            $requirement = isset($requirements[$name]) ? trim($requirements[$name], '^$') : '[^\\/]*';

            $parts[] = new ParameterPathPart($parameter, $requirement);
        }

        return new Path($parts);
    }
}
