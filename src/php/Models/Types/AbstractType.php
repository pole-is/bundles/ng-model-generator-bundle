<?php
declare(strict_types=1);
/*
 * This file is part of "irstea/ng-model-generator-bundle".
 *
 * "irstea/ng-model-generator-bundle" generates Typescript interfaces for Angular using api-platform metadata.
 * Copyright (C) 2018-2021 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\NgModelGeneratorBundle\Models\Types;

use Irstea\NgModelGeneratorBundle\Exceptions\DomainException;
use Irstea\NgModelGeneratorBundle\Exceptions\TypeError;

/**
 * Class AbstractType.
 */
abstract class AbstractType implements Type
{
    /**
     * @return string
     */
    public function __toString()
    {
        return sprintf('%s(#%x)', static::class, spl_object_hash($this));
    }

    /**
     * {@inheritdoc}
     */
    public function getIterator()
    {
        return new \EmptyIterator();
    }

    /**
     * {@inheritdoc}
     */
    public function castToStringOrStringArray(string $expr): string
    {
        throw new DomainException('cannot cast a ' . static::class . ' to string or string[]');
    }

    /**
     * {@inheritdoc}
     */
    public function checkType(string $expr, bool $explicit = false): string
    {
        throw new DomainException('cannot type-check a ' . static::class);
    }

    /**
     * @return mixed
     */
    public function jsonSerialize()
    {
        return $this->getUsage();
    }

    /**
     * {@inheritdoc}
     */
    public function getAsType(string $typeClass): Type
    {
        $type = $this->findType($typeClass);
        if (!$type) {
            throw new TypeError(sprintf('Cannot get type %s from %s', $typeClass, $this));
        }

        return $type;
    }

    /**
     * {@inheritdoc}
     */
    public function findType(string $typeClass): ?Type
    {
        if (is_a($this, $typeClass, true)) {
            return $this;
        }

        return null;
    }
}
