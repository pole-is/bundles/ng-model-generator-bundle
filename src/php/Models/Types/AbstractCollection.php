<?php
declare(strict_types=1);
/*
 * This file is part of "irstea/ng-model-generator-bundle".
 *
 * "irstea/ng-model-generator-bundle" generates Typescript interfaces for Angular using api-platform metadata.
 * Copyright (C) 2018-2021 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\NgModelGeneratorBundle\Models\Types;

/**
 * Class AbstractCollection.
 */
abstract class AbstractCollection extends AbstractType
{
    /**
     * @var Type
     */
    protected $valueType;

    /**
     * Collection constructor.
     */
    public function __construct(Type $valueType)
    {
        $this->valueType = $valueType;
    }

    public function getValueType(): Type
    {
        return $this->valueType;
    }

    /**
     * {@inheritdoc}
     */
    public function getIterator()
    {
        yield $this->getKeyType();
        yield $this->valueType;
    }

    /**
     * {@inheritdoc}
     */
    public function getUsage(): string
    {
        return sprintf('%s<%s>', $this->getGenericUsage(), $this->valueType->getUsage());
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return ['type' => $this->getGenericUsage(), 'keyType' => $this->getKeyType(), 'valueType' => $this->valueType];
    }

    abstract protected function getKeyType(): Type;

    abstract protected function getGenericUsage(): string;

    /**
     * {@inheritdoc}
     */
    public function findType(string $typeClass): ?Type
    {
        return parent::findType($typeClass) ?: $this->valueType->findType($typeClass);
    }
}
