<?php
declare(strict_types=1);
/*
 * This file is part of "irstea/ng-model-generator-bundle".
 *
 * "irstea/ng-model-generator-bundle" generates Typescript interfaces for Angular using api-platform metadata.
 * Copyright (C) 2018-2021 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\NgModelGeneratorBundle\Models\Types\Objects;

use Irstea\NgModelGeneratorBundle\Models\DeclarationTrait;
use Irstea\NgModelGeneratorBundle\Models\Types\Type;
use Irstea\NgModelGeneratorBundle\TypescriptHelper;

/**
 * Class Property.
 */
class Property implements \IteratorAggregate, \JsonSerializable
{
    use DeclarationTrait;

    /** @var Type */
    private $type;

    /** @var bool */
    private $isIdentifier = false;

    /** @var bool */
    private $isNullable = false;

    /** @var bool */
    private $isReadonly = false;

    /** @var string */
    private $originalName;

    /**
     * Property constructor.
     */
    public function __construct(
        string $name,
        string $description,
        Type $type,
        bool $isIdentifier = false,
        bool $isNullable = false,
        bool $isReadonly = false,
        string $originalName = null
    ) {
        $this->name = $name;
        $this->description = $description;
        $this->type = $type;
        $this->isIdentifier = $isIdentifier;
        $this->isNullable = $isNullable;
        $this->isReadonly = $isReadonly;
        $this->originalName = $originalName ?: $name;
    }

    /**
     * Get type.
     */
    public function getType(): Type
    {
        return $this->type;
    }

    /**
     * Get description.
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * Get originalName.
     */
    public function getOriginalName(): string
    {
        return $this->originalName;
    }

    /**
     * Get isIdentifier.
     */
    public function isIdentifier(): bool
    {
        return $this->isIdentifier;
    }

    /**
     * Get isNullable.
     */
    public function isNullable(): bool
    {
        return $this->isNullable;
    }

    /**
     * Get isReadonly.
     */
    public function isReadonly(): bool
    {
        return $this->isReadonly;
    }

    /**
     * {@inheritdoc}
     */
    public function getDeclaration(): string
    {
        return sprintf(
            '%s%s%s: %s',
            $this->isReadonly ? 'readonly ' : '',
            $this->getObjectKey(),
            $this->isNullable ? '?' : '',
            $this->getType()->getUsage()
        );
    }

    public function getObjectKey(bool $usingOriginal = false): string
    {
        return TypescriptHelper::objectLiteralKey($this->doGetName($usingOriginal));
    }

    /**
     * {@inheritdoc}
     */
    public function getUsage(string $varName, bool $usingOriginal = false): string
    {
        return TypescriptHelper::propertyAccessor($varName, $this->doGetName($usingOriginal));
    }

    private function doGetName(bool $usingOriginal = false): string
    {
        return $usingOriginal ? $this->originalName : $this->name;
    }

    /**
     * {@inheritdoc}
     */
    public function getIterator()
    {
        yield $this->getType();
    }

    /**
     * {@inheritdoc}
     */
    public function jsonSerialize()
    {
        return [
            'name'        => $this->name,
            'description' => $this->description,
            'type'        => $this->type,
            'identifier'  => $this->isIdentifier,
            'nullable'    => $this->isNullable,
            'readonly'    => $this->isReadonly,
        ];
    }

    public static function compare(self $a, self $b): bool
    {
        if ($a === $b) {
            return false;
        }
        if ($a->isIdentifier !== $b->isIdentifier) {
            return $b->isIdentifier;
        }
        if ($a->isReadonly !== $b->isReadonly) {
            return $b->isReadonly;
        }
        if ($a->isNullable !== $b->isNullable) {
            return $a->isNullable;
        }

        return $a->name > $b->name;
    }

    /**
     * @return static
     */
    public static function createIdentifier(string $name, string $description, Type $type)
    {
        return new self($name, $description, $type, true, false, true);
    }
}
