<?php
declare(strict_types=1);
/*
 * This file is part of "irstea/ng-model-generator-bundle".
 *
 * "irstea/ng-model-generator-bundle" generates Typescript interfaces for Angular using api-platform metadata.
 * Copyright (C) 2018-2021 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\NgModelGeneratorBundle\Models\Types\Objects;

use Irstea\NgModelGeneratorBundle\Exceptions\DomainException;
use Irstea\NgModelGeneratorBundle\Models\Types\AbstractType;
use Irstea\NgModelGeneratorBundle\Models\Types\BuiltinType;
use Irstea\NgModelGeneratorBundle\TypescriptHelper;

/**
 * Class AnonymousObject.
 */
class AnonymousObject extends AbstractType
{
    /**
     * @var Property[]
     */
    private $properties = [];

    /**
     * Resource constructor.
     *
     * @param Property[] $properties
     */
    public function __construct(array $properties)
    {
        foreach ($properties as $property) {
            $name = $property->getName();
            if (isset($this->properties[$name])) {
                throw new DomainException("duplicate object property: $name");
            }
            $this->properties[$name] = $property;
        }
        uasort($this->properties, [Property::class, 'compare']);
    }

    /**
     * @return Property[]
     */
    public function getProperties(): array
    {
        return $this->properties;
    }

    public function hasProperty(string $name): bool
    {
        $props = $this->getProperties();

        return isset($props[$name]);
    }

    public function getProperty(string $name): Property
    {
        $props = $this->getProperties();

        return $props[$name];
    }

    /**
     * {@inheritdoc}
     */
    public function getUsage(): string
    {
        return $this->getDeclarationBody(false);
    }

    /**
     * {@inheritdoc}
     */
    public function castToStringOrStringArray(string $expr): string
    {
        return sprintf('JSON.stringify(%s)', $expr);
    }

    /**
     * {@inheritdoc}
     */
    public function checkType(string $expr, bool $explicit = false): string
    {
        $tests = [BuiltinType::get('object')->checkType($expr, false)];

        /** @var Property $property */
        foreach ($this->properties as $property) {
            $tpl = $property->isNullable() ? '(!(%s) || %s)' : '(%s && %s)';
            $tests[] = sprintf(
                $tpl,
                TypescriptHelper::propertyTestor($expr, $property->getName()),
                $expr
            );
        }

        return '(' . implode("\n  && ", $tests) . ')';
    }

    /**
     * {@inheritdoc}
     */
    public function getDeclaration(): string
    {
        return trim(trim($this->getDeclarationHeader()) . ' ' . $this->getDeclarationBody(true));
    }

    protected function getDeclarationHeader(): string
    {
        return '';
    }

    private function getDeclarationBody(bool $multiline): string
    {
        $body = array_merge(
            $this->getPropertyDeclarations(),
            $this->getMethodDeclarations()
        );

        if ($multiline) {
            return "{\n" . TypescriptHelper::indent(implode("\n", $body)) . "\n}";
        }

        return '{ ' . implode(' ', $body) . ' }';
    }

    protected function getPropertyDeclarations(): array
    {
        return array_map(
            function (Property $p): string { return $p->getDeclaration() . ';'; },
            $this->getProperties()
        );
    }

    protected function getMethodDeclarations(): array
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function getIterator()
    {
        yield from $this->getProperties();
    }

    /**
     * {@inheritdoc}
     */
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}
