<?php
declare(strict_types=1);
/*
 * This file is part of "irstea/ng-model-generator-bundle".
 *
 * "irstea/ng-model-generator-bundle" generates Typescript interfaces for Angular using api-platform metadata.
 * Copyright (C) 2018-2021 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\NgModelGeneratorBundle\Models\Types\Objects;

use Irstea\NgModelGeneratorBundle\Models\Declaration;
use Irstea\NgModelGeneratorBundle\Models\DeclarationTrait;
use Irstea\NgModelGeneratorBundle\Models\Types\Type;
use Irstea\NgModelGeneratorBundle\TypescriptHelper;

/**
 * Class AbstractHierarchicalObject.
 */
abstract class AbstractHierarchicalObject extends AnonymousObject implements Declaration
{
    use DeclarationTrait;

    /** @var Type|null */
    protected $parent;

    /**
     * @var array
     */
    private $children;

    /**
     * ObjectClass constructor.
     *
     * @param Property[] $properties
     */
    public function __construct(string $name, ?Type $parent, array $properties = [], string $description = '', array $children = [])
    {
        parent::__construct($properties);

        $this->name = $name;
        $this->description = $description;
        $this->parent = $parent;
        $this->children = $children;
    }

    /**
     * {@inheritdoc}
     */
    final protected function getDeclarationHeader(): string
    {
        $parts = [];

        $comment = $this->getComment();
        if ($comment) {
            $parts[] = $comment;
        }

        $decorators = $this->getDecoratorDeclaration();
        if ($decorators) {
            $parts[] = $decorators;
        }

        $parts[] =
            trim(implode(' ', [$this->getQualifierDeclaration(), $this->getKind(), $this->name, $this->getInheritanceDeclaration()]));

        return implode("\n", array_filter($parts));
    }

    protected function getComment(): string
    {
        return $this->description
             ? sprintf("/**\n%s\n */", TypescriptHelper::indent($this->description, ' * '))
             : '';
    }

    protected function getDecoratorDeclaration(): string
    {
        return '';
    }

    protected function getQualifierDeclaration(): string
    {
        return 'export';
    }

    protected function getInheritanceDeclaration(): string
    {
        return $this->parent ? sprintf('extends %s', $this->parent->getUsage()) : '';
    }

    /**
     * {@inheritdoc}
     */
    public function getIterator()
    {
        if ($this->parent) {
            yield $this->parent;
        }
        yield from $this->children;
        yield from parent::getIterator();
    }

    abstract protected function getKind(): string;
}
