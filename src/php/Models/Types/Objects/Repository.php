<?php
declare(strict_types=1);
/*
 * This file is part of "irstea/ng-model-generator-bundle".
 *
 * "irstea/ng-model-generator-bundle" generates Typescript interfaces for Angular using api-platform metadata.
 * Copyright (C) 2018-2021 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\NgModelGeneratorBundle\Models\Types\Objects;

use Irstea\NgModelGeneratorBundle\Iterators\IteratorBuilder;
use Irstea\NgModelGeneratorBundle\Models\ClassName;
use Irstea\NgModelGeneratorBundle\Models\Types\Operations\Operation;
use Irstea\NgModelGeneratorBundle\Models\Types\Operations\Path;
use Irstea\NgModelGeneratorBundle\Models\Types\Reference;
use Irstea\NgModelGeneratorBundle\Models\Types\Resources\Representation;
use Irstea\NgModelGeneratorBundle\Models\Types\StringConst;
use Irstea\NgModelGeneratorBundle\Models\Types\Type;
use Irstea\NgModelGeneratorBundle\Models\Types\Union;

/**
 * Class Repository.
 */
final class Repository extends ClassType
{
    /** @var ClassName */
    private $resource;

    /** @var Operation[] */
    private $operations = [];

    /** @var Path */
    private $iri;

    /** @var Type */
    private $resourceType;

    /** @var Property */
    private $identifier;

    /**
     * Repository constructor.
     *
     * @param Operation[] $operations
     */
    public function __construct(ClassName $resource, Type $resourceType, Property $identifier, Path $iri, array $operations, string $description = '')
    {
        parent::__construct(
            $resource->getBaseName() . 'Repository',
            null,
            [],
            sprintf('Repository de %s', $resource->getBaseName()) . $description
        );

        $this->resource = $resource;

        foreach ($operations as $operation) {
            $this->operations[$operation->getName()] = $operation;
        }

        ksort($this->operations);
        $this->resourceType = $resourceType;
        $this->identifier = $identifier;
        $this->iri = $iri;
    }

    /**
     * Get resourceName.
     */
    public function getResourceName(): string
    {
        return $this->resource->getBaseName();
    }

    /**
     * Get resourceType.
     */
    public function getResource(): Type
    {
        return $this->resourceType;
    }

    /**
     * Get iri.
     */
    public function getIRI(): Path
    {
        return $this->iri;
    }

    public function getIdentifier(): Property
    {
        return $this->identifier;
    }

    /**
     * {@inheritdoc}
     */
    protected function getInheritanceDeclaration(): string
    {
        return sprintf(' implements Resolve<%s>', $this->getResourceName());
    }

    /**
     * {@inheritdoc}
     */
    protected function getDecoratorDeclaration(): string
    {
        return '@Injectable()';
    }

    /**
     * Get operations.
     *
     * @return Operation[]
     */
    public function getOperations(): array
    {
        return $this->operations;
    }

    public static function compare(self $a, self $b): bool
    {
        return $a->getResourceName() > $b->getResourceName();
    }

    /**
     * {@inheritdoc}
     */
    public function getIterator()
    {
        yield from parent::getIterator();
        yield $this->resourceType;
        yield from $this->operations;
    }

    public function getAtTypes(): \Iterator
    {
        return IteratorBuilder::from($this->resourceType)
            ->unique()
            ->recurseWhere(
                \RecursiveIteratorIterator::LEAVES_ONLY,
                function ($obj): bool {
                    if ($obj instanceof Property && $obj->getName() === '@type') {
                        return true;
                    }

                    return $obj instanceof Union || $obj instanceof Reference || $obj instanceof Representation;
                }
            )->where(function ($obj): bool {
                return $obj instanceof StringConst;
            })->getIterator();
    }
}
