<?php
declare(strict_types=1);
/*
 * This file is part of "irstea/ng-model-generator-bundle".
 *
 * "irstea/ng-model-generator-bundle" generates Typescript interfaces for Angular using api-platform metadata.
 * Copyright (C) 2018-2021 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\NgModelGeneratorBundle\Models\Types;

use Irstea\NgModelGeneratorBundle\Models\Declaration;
use Irstea\NgModelGeneratorBundle\Models\DeclarationTrait;
use Irstea\NgModelGeneratorBundle\TypescriptHelper;

/**
 * Class Alias.
 */
final class Alias extends AbstractType implements Declaration
{
    use DeclarationTrait;

    /** @var Type */
    private $target;

    /**
     * Alias constructor.
     */
    public function __construct(string $name, Type $target, string $description = '')
    {
        $this->name = $name;
        $this->target = $target;
        $this->description = $description;
    }

    /**
     * {@inheritdoc}
     */
    public function getDeclaration(): string
    {
        return sprintf(
            '%sexport type %s = %s;',
            $this->description
             ? sprintf("/**\n%s\n */\n", TypescriptHelper::indent($this->description, ' * '))
             : '',
            $this->name,
            $this->target->getUsage()
        );
    }

    /**
     * {@inheritdoc}
     */
    public function castToStringOrStringArray(string $expr): string
    {
        return $this->target->castToStringOrStringArray($expr);
    }

    /**
     * {@inheritdoc}
     */
    public function checkType(string $expr, bool $explicit = false): string
    {
        return $this->target->checkType($expr, $explicit);
    }

    /**
     * {@inheritdoc}
     */
    public function getIterator()
    {
        yield $this->target;
    }

    /**
     * {@inheritdoc}
     */
    public function findType(string $typeClass): ?Type
    {
        return parent::findType($typeClass) ?: $this->target->findType($typeClass);
    }
}
