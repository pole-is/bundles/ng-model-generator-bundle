<?php
declare(strict_types=1);
/*
 * This file is part of "irstea/ng-model-generator-bundle".
 *
 * "irstea/ng-model-generator-bundle" generates Typescript interfaces for Angular using api-platform metadata.
 * Copyright (C) 2018-2021 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\NgModelGeneratorBundle\Models\Types\Operations;

use Irstea\NgModelGeneratorBundle\Models\DeclarationTrait;
use Irstea\NgModelGeneratorBundle\Models\Types\AbstractCollection;
use Irstea\NgModelGeneratorBundle\Models\Types\BuiltinType;
use Irstea\NgModelGeneratorBundle\Models\Types\Reference;
use Irstea\NgModelGeneratorBundle\Models\Types\Resources\Representation;
use Irstea\NgModelGeneratorBundle\Models\Types\Type;

/**
 * Class Operation.
 */
final class Operation implements \IteratorAggregate, \JsonSerializable
{
    use DeclarationTrait;

    /**
     * @var Parameter[]
     */
    private $parameters;

    /** @var Type */
    private $returnType;

    /** @var string[] */
    private $body;

    /** @var Type[] */
    private $relatedTypes;

    /** @var Path */
    private $path;

    /**
     * Operation constructor.
     *
     * @param Parameter[] $parameters
     * @param string[]    $body
     */
    public function __construct(
        string $name,
        array $parameters,
        Path $path,
        ?Type $returnType,
        array $body,
        string $description = '',
        array $relatedTypes = []
    ) {
        $this->name = $name;
        $this->description = $description;
        $this->parameters = $parameters;
        $this->returnType = $returnType ?: BuiltinType::get('void');
        $this->body = $body;
        $this->relatedTypes = $relatedTypes;
        $this->path = $path;
    }

    /**
     * Get path.
     *
     * @return Path
     */
    public function getPath(): ?Path
    {
        return $this->path;
    }

    /**
     * Get parameters.
     *
     * @return Parameter[]
     */
    public function getParameters(): array
    {
        return $this->parameters;
    }

    /**
     * Get returnType.
     */
    public function getReturnType(): Type
    {
        return $this->returnType;
    }

    /**
     * Get body.
     *
     * @return string[]
     */
    public function getBody(): array
    {
        return $this->body;
    }

    public function getResourceType(): ?Representation
    {
        $type = $this->returnType;

        while ($type instanceof Reference || $type instanceof AbstractCollection) {
            if ($type instanceof Reference) {
                $type = $type->getTarget();
            } else {
                $type = $type->getValueType();
            }
        }

        return $type instanceof Representation ? $type : null;
    }

    /**
     * {@inheritdoc}
     */
    public function getIterator()
    {
        yield $this->returnType;
        yield from $this->parameters;
        yield from $this->relatedTypes;
    }

    /**
     * {@inheritdoc}
     */
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}
