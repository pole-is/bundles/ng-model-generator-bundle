<?php
declare(strict_types=1);
/*
 * This file is part of "irstea/ng-model-generator-bundle".
 *
 * "irstea/ng-model-generator-bundle" generates Typescript interfaces for Angular using api-platform metadata.
 * Copyright (C) 2018-2021 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\NgModelGeneratorBundle\Models\Types\Operations;

use Irstea\NgModelGeneratorBundle\Models\NamedTrait;
use Irstea\NgModelGeneratorBundle\Models\Types\Type;

/**
 * Class Parameter.
 */
class Parameter implements \IteratorAggregate, \JsonSerializable
{
    use NamedTrait;

    /**
     * @var Type
     */
    private $type;

    /**
     * @var bool
     */
    private $optional = false;

    /**
     * @var string|null
     */
    private $default;

    /**
     * Parameter constructor.
     */
    public function __construct(string $name, Type $type, bool $optional = false, ?string $default = null)
    {
        $this->name = $name;
        $this->type = $type;
        $this->optional = $optional;
        $this->default = $default;
    }

    /**
     * Get type.
     */
    public function getType(): Type
    {
        return $this->type;
    }

    /**
     * {@inheritdoc}
     */
    public function getDeclaration(): string
    {
        $parts = [$this->name];
        if ($this->optional) {
            $parts[] = '?';
        }
        $parts[] = ': ';
        $parts[] = $this->type->getUsage();
        if ($this->default !== null) {
            $parts[] = ' = ';
            $parts[] = $this->default;
        }

        return implode('', $parts);
    }

    /**
     * Get optional.
     */
    public function isOptional(): bool
    {
        return $this->optional;
    }

    /**
     * {@inheritdoc}
     */
    public function getIterator()
    {
        yield $this->type;
    }

    public function isEqual(self $other): bool
    {
        /* @noinspection TypeUnsafeComparisonInspection */
        return $other instanceof self &&
            $other->name === $this->name &&
            $other->optional === $this->optional &&
            $other->type == $this->type;
    }

    /**
     * @return array|mixed
     */
    public function jsonSerialize()
    {
        return [
            'name'     => $this->name,
            'type'     => $this->type,
            'required' => !$this->optional,
        ];
    }
}
