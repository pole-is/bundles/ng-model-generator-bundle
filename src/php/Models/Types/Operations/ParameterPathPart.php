<?php
declare(strict_types=1);
/*
 * This file is part of "irstea/ng-model-generator-bundle".
 *
 * "irstea/ng-model-generator-bundle" generates Typescript interfaces for Angular using api-platform metadata.
 * Copyright (C) 2018-2021 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\NgModelGeneratorBundle\Models\Types\Operations;

/**
 * Class ParameterPathPart.
 */
final class ParameterPathPart implements PathPart
{
    /**
     * @var Parameter
     */
    private $parameter;

    /**
     * @var string
     */
    private $requirement;

    /**
     * ParameterPathPart constructor.
     */
    public function __construct(Parameter $parameter, string $requirement = '[^/]+')
    {
        $this->parameter = $parameter;
        $this->requirement = $requirement;
    }

    /**
     * {@inheritdoc}
     */
    public function asTemplate(): string
    {
        return sprintf('${%s}', $this->parameter->getName());
    }

    /**
     * {@inheritdoc}
     */
    public function asTestPattern(): string
    {
        return $this->requirement;
    }

    /**
     * {@inheritdoc}
     */
    public function asCapturePattern(): string
    {
        return sprintf('(%s)', $this->requirement);
    }

    /**
     * {@inheritdoc}
     */
    public function getParameter(): ?Parameter
    {
        return $this->parameter;
    }

    /**
     * {@inheritdoc}
     */
    public function isEqual(PathPart $other): bool
    {
        return $other instanceof self && $other->parameter->isEqual($this->parameter);
    }

    /**
     * @return \Generator
     */
    public function getIterator()
    {
        yield $this->parameter;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}
