<?php
declare(strict_types=1);
/*
 * This file is part of "irstea/ng-model-generator-bundle".
 *
 * "irstea/ng-model-generator-bundle" generates Typescript interfaces for Angular using api-platform metadata.
 * Copyright (C) 2018-2021 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\NgModelGeneratorBundle\Models\Types\Operations;

use Irstea\NgModelGeneratorBundle\TypescriptHelper;

/**
 * Class Path.
 */
final class Path
{
    /**
     * @var PathPart[]
     */
    private $parts;

    /**
     * Path constructor.
     *
     * @param PathPart[] $parts
     */
    public function __construct(array $parts)
    {
        $this->parts = $parts;
    }

    public function getUsage(): string
    {
        return TypescriptHelper::quoteString(
            $this->getTemplate(),
            $this->hasParameters() ? '`' : "'"
        );
    }

    public function getTemplate(): string
    {
        return implode(
            '',
            array_map(
                function (PathPart $part): string {
                    return $part->asTemplate();
                },
                $this->parts
            )
        );
    }

    public function getTestPattern(): string
    {
        return implode(
            '',
            array_map(
                function (PathPart $part): string {
                    return $part->asTestPattern();
                },
                $this->parts
            )
        );
    }

    public function getCapturePattern(): string
    {
        return implode(
            '',
            array_map(
                function (PathPart $part): string {
                    return $part->asCapturePattern();
                },
                $this->parts
            )
        );
    }

    public function hasParameters(): bool
    {
        foreach ($this->parts as $part) {
            if ($part->getParameter() !== null) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return Parameter[]
     */
    public function getParameters(): array
    {
        return array_filter(
            array_map(
                function (PathPart $part): ?Parameter {
                    return $part->getParameter();
                },
                $this->parts
            )
        );
    }

    public function buildUsing(self $other, Parameter $otherParameter): self
    {
        if (\count($other->parts) > \count($this->parts)) {
            return $this;
        }
        foreach ($other->parts as $index => $part) {
            if (!$this->parts[$index]->isEqual($part)) {
                return $this;
            }
        }
        if (\count($other->parts) === \count($this->parts)) {
            return $other;
        }

        return new self(
            array_merge(
                [new ParameterPathPart($otherParameter, $other->getTestPattern())],
                \array_slice($this->parts, \count($other->parts))
            )
        );
    }

    /**
     * @return \Generator
     */
    public function getIterator()
    {
        yield from $this->parts;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}
