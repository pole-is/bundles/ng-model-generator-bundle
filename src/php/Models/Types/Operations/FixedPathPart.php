<?php
declare(strict_types=1);
/*
 * This file is part of "irstea/ng-model-generator-bundle".
 *
 * "irstea/ng-model-generator-bundle" generates Typescript interfaces for Angular using api-platform metadata.
 * Copyright (C) 2018-2021 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\NgModelGeneratorBundle\Models\Types\Operations;

/**
 * Class FixedPathPart.
 */
class FixedPathPart implements PathPart
{
    /**
     * @var string
     */
    private $part;

    /**
     * FixedPathPart constructor.
     */
    public function __construct(string $part)
    {
        $this->part = $part;
    }

    /**
     * {@inheritdoc}
     */
    public function asTemplate(): string
    {
        return $this->part;
    }

    /**
     * {@inheritdoc}
     */
    public function asTestPattern(): string
    {
        return preg_quote($this->part, '/');
    }

    /**
     * {@inheritdoc}
     */
    public function asCapturePattern(): string
    {
        return $this->asTestPattern();
    }

    /**
     * {@inheritdoc}
     */
    public function getParameter(): ?Parameter
    {
        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function isEqual(PathPart $other): bool
    {
        return $other instanceof self && $other->part === $this->part;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}
