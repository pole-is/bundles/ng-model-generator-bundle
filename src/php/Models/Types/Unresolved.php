<?php
declare(strict_types=1);
/*
 * This file is part of "irstea/ng-model-generator-bundle".
 *
 * "irstea/ng-model-generator-bundle" generates Typescript interfaces for Angular using api-platform metadata.
 * Copyright (C) 2018-2021 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\NgModelGeneratorBundle\Models\Types;

use Irstea\NgModelGeneratorBundle\Models\HasName;

/**
 * Class Unresolved.
 */
final class Unresolved implements Type, HasName
{
    /**
     * Unresolved constructor.
     */
    private function __construct()
    {
    }

    /**
     * {@inheritdoc}
     */
    public function getIterator()
    {
        return new \EmptyIterator();
    }

    /**
     * {@inheritdoc}
     */
    public function getName(): string
    {
        return 'unresolved';
    }

    /**
     * {@inheritdoc}
     */
    public function getDeclaration(): string
    {
        return '';
    }

    /**
     * {@inheritdoc}
     */
    public function getDescription(): string
    {
        return '/* Unresolved */';
    }

    /**
     * {@inheritdoc}
     */
    public function getUsage(): string
    {
        return 'never /* unresolved */';
    }

    /**
     * {@inheritdoc}
     */
    public function castToStringOrStringArray(string $expr): string
    {
        return "'' /* unresolved */";
    }

    /**
     * {@inheritdoc}
     */
    public function checkType(string $expr, bool $explicit = false): string
    {
        return 'false /* unresolved */';
    }

    /**
     * {@inheritdoc}
     */
    public function jsonSerialize()
    {
        return 'unresolved';
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getUsage();
    }

    public static function get(): self
    {
        static $instance = null;

        if (!$instance) {
            $instance = new self();
        }

        return $instance;
    }

    /**
     * {@inheritdoc}
     */
    public function getAsType(string $typeClass): Type
    {
        throw new \TypeError(__CLASS__ . " cannot be cast as $typeClass");
    }

    /**
     * {@inheritdoc}
     */
    public function findType(string $typeClass): ?Type
    {
        return null;
    }
}
