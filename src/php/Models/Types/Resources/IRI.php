<?php
declare(strict_types=1);
/*
 * This file is part of "irstea/ng-model-generator-bundle".
 *
 * "irstea/ng-model-generator-bundle" generates Typescript interfaces for Angular using api-platform metadata.
 * Copyright (C) 2018-2021 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\NgModelGeneratorBundle\Models\Types\Resources;

use Irstea\NgModelGeneratorBundle\Models\Types\AbstractType;
use Irstea\NgModelGeneratorBundle\Models\Types\Type;

/**
 * Class IRI.
 */
final class IRI extends AbstractType
{
    /** @var Type[] */
    private $resources;

    /**
     * IRI constructor.
     *
     * @param Type[] $resources
     */
    public function __construct(array $resources)
    {
        $this->resources = $resources;
    }

    /**
     * @return string[]
     */
    private function getNames(): array
    {
        return array_map(
            function (Type $t): string {
                return $t->getUsage();
            },
            $this->resources
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getUsage(): string
    {
        return sprintf('IRI<%s>', implode(' | ', $this->getNames()));
    }

    /**
     * {@inheritdoc}
     */
    public function castToStringOrStringArray(string $expr): string
    {
        return sprintf('%s.toString()', $expr);
    }

    /**
     * {@inheritdoc}
     */
    public function checkType(string $expr, bool $explicit = false): string
    {
        return sprintf("(typeof %s === 'string')", $expr);
    }

    /**
     * {@inheritdoc}
     */
    public function getIterator()
    {
        yield from $this->resources;
    }
}
