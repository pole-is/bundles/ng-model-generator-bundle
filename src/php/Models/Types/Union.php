<?php
declare(strict_types=1);
/*
 * This file is part of "irstea/ng-model-generator-bundle".
 *
 * "irstea/ng-model-generator-bundle" generates Typescript interfaces for Angular using api-platform metadata.
 * Copyright (C) 2018-2021 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\NgModelGeneratorBundle\Models\Types;

/**
 * Class Union.
 */
final class Union extends AbstractType
{
    /**
     * @var Type[]
     */
    private $types = [];

    /**
     * Union constructor.
     */
    private function __construct(array $types = [])
    {
        $this->types = array_values($types);
    }

    /**
     * {@inheritdoc}
     */
    public function getUsage(): string
    {
        $reprs = [];
        foreach ($this->flatten() as $type) {
            $reprs[$type->getUsage()] = true;
        }
        ksort($reprs);

        return implode(' | ', array_keys($reprs));
    }

    /**
     * {@inheritdoc}
     */
    public function castToStringOrStringArray(string $expr): string
    {
        $alternatives = $this->types;
        $first = array_shift($alternatives);
        $cast = $first->castToStringOrStringArray($expr);
        foreach ($alternatives as $type) {
            $typeCast = $type->castToStringOrStringArray($expr);
            if ($typeCast === $cast) {
                continue;
            }
            $check = $type->checkType($expr);
            if ($check) {
                $cast = sprintf('(%s ? %s : %s)', $check, $typeCast, $cast);
            }
        }

        return $cast;
    }

    /**
     * {@inheritdoc}
     */
    public function checkType(string $expr, bool $explicit = false): string
    {
        $checks = [];
        foreach ($this->types as $type) {
            $checks[$type->checkType($expr, $explicit)] = true;
        }

        return '(' . implode(' || ', array_keys($checks)) . ')';
    }

    /**
     * @return \Generator|Type[]
     */
    protected function flatten(): \Generator
    {
        foreach ($this->types as $type) {
            if ($type instanceof self) {
                yield from $type->flatten();
            } else {
                yield $type;
            }
        }
    }

    /**
     * @param Type[] $types
     */
    public static function create(array $types): Type
    {
        switch (\count($types)) {
            case 0:
                return BuiltinType::get('never');
            case 1:
                return $types[0];
            default:
                return new self($types);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getIterator()
    {
        yield from $this->types;
    }

    /**
     * {@inheritdoc}
     */
    public function jsonSerialize()
    {
        return [
            'type'  => 'union',
            'types' => $this->types,
        ];
    }
}
