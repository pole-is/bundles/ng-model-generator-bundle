<?php
declare(strict_types=1);
/*
 * This file is part of "irstea/ng-model-generator-bundle".
 *
 * "irstea/ng-model-generator-bundle" generates Typescript interfaces for Angular using api-platform metadata.
 * Copyright (C) 2018-2021 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\NgModelGeneratorBundle\Models\Types;

/**
 * Class ArrayType.
 */
class ArrayType extends AbstractCollection
{
    /**
     * {@inheritdoc}
     */
    protected function getKeyType(): Type
    {
        return BuiltinType::get('number');
    }

    /**
     * {@inheritdoc}
     */
    protected function getGenericUsage(): string
    {
        return 'Array';
    }

    /**
     * {@inheritdoc}
     */
    public function castToStringOrStringArray(string $expr): string
    {
        $itemName = $this->getItemName();
        $cast = $this->valueType->castToStringOrStringArray($itemName);
        if ($cast !== $itemName) {
            return sprintf('%s.map(%s => %s)', $expr, $itemName, $cast);
        }

        return $expr;
    }

    /**
     * {@inheritdoc}
     */
    public function checkType(string $expr, bool $explicit = false): string
    {
        $itemName = $this->getItemName();

        return sprintf(
            '(Array.isArray(%s) && %s.every(%s => %s))',
            $expr,
            $expr,
            $itemName,
            $this->valueType->checkType($itemName, $explicit)
        );
    }

    private function getItemName(): string
    {
        return lcfirst(preg_replace('/\W+/', '', $this->getValueType()->getUsage()));
    }
}
