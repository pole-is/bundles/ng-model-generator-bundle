<?php
declare(strict_types=1);
/*
 * This file is part of "irstea/ng-model-generator-bundle".
 *
 * "irstea/ng-model-generator-bundle" generates Typescript interfaces for Angular using api-platform metadata.
 * Copyright (C) 2018-2021 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\NgModelGeneratorBundle\Models\Types;

use Irstea\NgModelGeneratorBundle\Models\MultitonTrait;
use Irstea\NgModelGeneratorBundle\Models\NamedTrait;
use Irstea\NgModelGeneratorBundle\TypescriptHelper;

/**
 * Class BuiltinType.
 */
class BuiltinType extends AbstractType
{
    use MultitonTrait;
    use NamedTrait;

    /**
     * BuiltinType constructor.
     */
    private function __construct(string $name)
    {
        $this->name = $name;
    }

    /**
     * {@inheritdoc}
     */
    public function castToStringOrStringArray(string $expr): string
    {
        switch ($this->name) {
            case 'string':
                return $expr;
            case 'number':
            case 'boolean':
                return sprintf('%s.toString()', $expr);
            case 'null':
            case 'undefined':
            case 'true':
            case 'false':
                return sprintf('"%s"', $this->name);
            case 'Date':
                return sprintf('%s.toISOString()', $expr);
            case 'never':
            case 'unknown':
                return sprintf('"" /* %s */', $this->name);
            default:
                return sprintf('JSON.stringify(%s)', $expr);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function checkType(string $expr, bool $explicit = false): string
    {
        switch ($this->name) {
            case 'null':
            case 'true':
            case 'false':
            case 'undefined':
                return sprintf('(%s === %s)', $expr, $this->name);
            case 'unknown':
            case 'any':
                return 'true';
            case 'Date':
                return sprintf('(%s instanceof %s)', $expr, $this->name);
            default:
                return sprintf('(typeof %s === %s)', $expr, TypescriptHelper::quoteString($this->name));
        }
    }
}
