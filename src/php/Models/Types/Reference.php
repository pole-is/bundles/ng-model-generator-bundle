<?php
declare(strict_types=1);
/*
 * This file is part of "irstea/ng-model-generator-bundle".
 *
 * "irstea/ng-model-generator-bundle" generates Typescript interfaces for Angular using api-platform metadata.
 * Copyright (C) 2018-2021 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\NgModelGeneratorBundle\Models\Types;

use Irstea\NgModelGeneratorBundle\Exceptions\DomainException;

/**
 * Class Ref.
 */
class Reference extends AbstractType implements Deferred
{
    private const UNRESOLVED = 0;
    private const RESOLVING = 1;
    private const RESOLVED = 2;

    /** @var Type */
    private $target;

    /** @var string */
    private $name;

    /** @var int */
    private $state = self::UNRESOLVED;

    /**
     * Reference constructor.
     */
    public function __construct(string $name = 'anonymous reference')
    {
        $this->name = $name;
        $this->target = Unresolved::get();
    }

    /**
     * Get target.
     */
    public function getTarget(): Type
    {
        return $this->target;
    }

    /**
     * {@inheritdoc}
     */
    public function resolveWith(callable $callback): Type
    {
        switch ($this->state) {
            case self::UNRESOLVED:
                $this->state = self::RESOLVING;

                return $this->resolve($callback());
            case self::RESOLVING:
                return $this;
            case self::RESOLVED:
                return $this->target;
            default:
                throw new DomainException('invalid reference state: ' . $this->state);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function resolve(Type $result): Type
    {
        if ($this->state !== self::RESOLVED) {
            $this->state = self::RESOLVED;
            $this->target = $result;
        }

        return $this->target;
    }

    /**
     * {@inheritdoc}
     */
    private function dereference(): Type
    {
        return $this->target instanceof self ? $this->target->dereference() : $this->target;
    }

    /**
     * {@inheritdoc}
     */
    public function getUsage(): string
    {
        return $this->dereference()->getUsage();
    }

    /**
     * {@inheritdoc}
     */
    public function castToStringOrStringArray(string $expr): string
    {
        return $this->dereference()->castToStringOrStringArray($expr);
    }

    /**
     * {@inheritdoc}
     */
    public function checkType(string $expr, bool $explicit = false): string
    {
        return $this->dereference()->checkType($expr, $explicit);
    }

    /**
     * {@inheritdoc}
     */
    public function getIterator()
    {
        yield $this->target;
    }

    /**
     * @return mixed
     */
    public function jsonSerialize()
    {
        return spl_object_hash($this);
    }

    /**
     * {@inheritdoc}
     */
    public function __toString()
    {
        return '&' . $this->name;
    }

    /**
     * {@inheritdoc}
     */
    public function findType(string $typeClass): ?Type
    {
        return parent::findType($typeClass) ?: $this->target->findType($typeClass);
    }
}
