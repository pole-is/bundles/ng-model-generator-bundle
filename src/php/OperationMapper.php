<?php
declare(strict_types=1);
/*
 * This file is part of "irstea/ng-model-generator-bundle".
 *
 * "irstea/ng-model-generator-bundle" generates Typescript interfaces for Angular using api-platform metadata.
 * Copyright (C) 2018-2021 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\NgModelGeneratorBundle;

use Assert\Assertion;
use Doctrine\Common\Inflector\Inflector;
use Irstea\NgModelGeneratorBundle\Exceptions\InvalidArgumentException;
use Irstea\NgModelGeneratorBundle\Metadata\OperationMetadata;
use Irstea\NgModelGeneratorBundle\Metadata\SerializationMetadata;
use Irstea\NgModelGeneratorBundle\Models\PHPClass;
use Irstea\NgModelGeneratorBundle\Models\Types\AbstractCollection;
use Irstea\NgModelGeneratorBundle\Models\Types\ArrayType;
use Irstea\NgModelGeneratorBundle\Models\Types\BuiltinType;
use Irstea\NgModelGeneratorBundle\Models\Types\Objects\AnonymousObject;
use Irstea\NgModelGeneratorBundle\Models\Types\Objects\InterfaceType;
use Irstea\NgModelGeneratorBundle\Models\Types\Objects\Property;
use Irstea\NgModelGeneratorBundle\Models\Types\Operations\Operation;
use Irstea\NgModelGeneratorBundle\Models\Types\Operations\Parameter;
use Irstea\NgModelGeneratorBundle\Models\Types\Operations\Path;
use Irstea\NgModelGeneratorBundle\Models\Types\Placeholder;
use Irstea\NgModelGeneratorBundle\Models\Types\Reference;
use Irstea\NgModelGeneratorBundle\Models\Types\Resources\Collection;
use Irstea\NgModelGeneratorBundle\Models\Types\Resources\Representation;
use Irstea\NgModelGeneratorBundle\Models\Types\Type;
use Irstea\NgModelGeneratorBundle\Models\Types\Union;

/**
 * Class OperationMapper.
 */
final class OperationMapper
{
    /** @var TypeFactoryInterface */
    private $typeFactory;

    /** @var OperationMetadata */
    private $operation;

    /** @var PathParserInterface */
    private $pathParser;

    /** @var SerializationMapperFactoryInterface */
    private $serializationMapperFactory;

    /** @var Path|null */
    private $iri;

    /**
     * OperationMapper constructor.
     */
    public function __construct(
        TypeFactoryInterface $typeFactory,
        SerializationMapperFactoryInterface $serializationMapperFactory,
        PathParserInterface $pathParser,
        OperationMetadata $operation,
        ?Path $iri
    ) {
        $this->typeFactory = $typeFactory;
        $this->serializationMapperFactory = $serializationMapperFactory;
        $this->operation = $operation;
        $this->pathParser = $pathParser;
        $this->iri = $iri;
    }

    public function __invoke(): Operation
    {
        $responseBody = null;

        $normalization = $this->operation->getNormalization();
        if ($normalization) {
            $responseBody = $this->mapSerialization($normalization);

            if ($this->operation->isCollectionOperation()) {
                $responseBody = new Collection($responseBody);
            }
        }

        $opParameters = $body = [];
        $callParameters = [TypescriptHelper::quoteString($this->operation->getMethod())];

        $callPath = $path = $this->pathParser->parse($this->operation->getPath(), $this->operation->getRequirements());

        $iriParameter = $iriParameterName = null;

        if ($this->iri) {
            $iriParameter = new Parameter('iri', Placeholder::get(sprintf('IRI<%s>', $this->operation->getClassName())));

            $callPath = $path->buildUsing($this->iri, $iriParameter);
        }

        if ($callPath === $this->iri) {
            Assertion::notNull($iriParameter);
            $opParameters[] = $iriParameter;
            $iriParameterName = $iriParameter->getName();
            $callParameters[] = $iriParameterName . ' as any';
        } else {
            foreach ($callPath->getParameters() as $parameter) {
                $opParameters[] = $parameter;
            }
            $body[] = 'const path = ' . $callPath->getUsage() . ';';
            $callParameters[] = 'path';
        }

        $denormalization = $this->operation->getDenormalization();
        if ($denormalization) {
            $requestBody = $this->mapSerialization($denormalization);

            if ($this->operation->isCollectionOperation()) {
                $requestBody = new ArrayType($requestBody);
            }

            $opParameters[] = new Parameter('body', $requestBody);
            $body[] = 'options.body = body;';
        }

        $filterProperties = $this->getFilterProperties();
        if ($filterProperties) {
            [$filterBody, $filterParam] = $this->buildFilterBody($filterProperties, 'options.params');
            $body = array_merge($body, $filterBody);
            $opParameters[] = $filterParam;
        }

        $opParameters[] = new Parameter('options', Placeholder::get('RequestOptions'), false, '{}');
        $callParameters[] = 'options';

        $returnType = $responseBody ?: Placeholder::get('HttpResponseBase');

        $body[] = sprintf('return %s;', $this->buildClientCall($callParameters, $returnType, $iriParameterName));

        return new Operation(
            $this->operation->getName(),
            $opParameters,
            $path,
            $returnType,
            $body,
            sprintf(
                "Operation: %s\nType: %s\nMethod: %s\nPath: %s",
                $this->operation->getOpDef()->getOriginalName(),
                $this->operation->getType(),
                $this->operation->getMethod(),
                $this->operation->getPath()
            )
        );
    }

    /***
     * @param SerializationMetadata $serializationMetadata
     *
     * @return Type
     */
    private function mapSerialization(SerializationMetadata $serializationMetadata): Type
    {
        $mapper = $this->serializationMapperFactory->create(
            $serializationMetadata,
            !$this->operation->getOpDef()->isCreateItem()
        );

        return $mapper->get($serializationMetadata->getRoot()->getFullName());
    }

    private function buildClientCall(array $callParameters, Type $returnType, ?string $iriParameterName): string
    {
        $clientCall = sprintf(
            'this.client.request<%s>(%s)',
            $returnType->getUsage(),
            implode(', ', $callParameters)
        );

        /** @var Representation|null $repr */
        $repr = $returnType->findType(Representation::class);
        if ($repr === null || !$repr->hasProperty('@id')) {
            return $clientCall;
        }

        $opDef = $this->operation->getOpDef();

        if ($opDef->isGetCollection()) {
            return "this.cache.getAll($clientCall)";
        }
        if ($opDef->isCreateItem()) {
            return "this.cache.post($clientCall)";
        }
        if ($iriParameterName) {
            if ($opDef->isGetItem()) {
                return "this.cache.get($iriParameterName, () => $clientCall)";
            }
            if ($opDef->isUpdateItem()) {
                return "this.cache.put($iriParameterName, $clientCall)";
            }
            if ($opDef->isDeleteItem()) {
                return "this.cache.delete($iriParameterName, $clientCall)";
            }
        }

        return $clientCall;
    }

    /**
     * @return Property[]
     */
    private function getFilterProperties(): array
    {
        /** @var Property[] $properties */
        $properties = [];

        $pagination = $this->operation->getPagination();
        if ($pagination && $pagination->isEnabled()) {
            $intType = BuiltinType::get('number');

            $pageName = $pagination->getPageParameterName();
            $properties[] = new Property($pageName, 'Requested page', $intType, false, true, false, $pageName);

            if ($pagination->isClientItemsPerPage()) {
                $pageSizeName = $pagination->getItemsPerPageParameterName();
                $properties[] = new Property($pageSizeName, 'Page size', $intType, false, true, false, $pageSizeName);
            }
        }

        $rootClass = $this->operation->getClassName();

        foreach ($this->operation->getFilters() as $filter) {
            $filterType = PHPClass::get(\get_class($filter))->getBaseName();

            foreach ($filter->getDescription($this->operation->getResource()->getFullName()) as $name => $filterDesc) {
                $type = $this->typeFactory->get($filterDesc['type']);

                if (isset($filterDesc['property']) && \is_string($filterDesc['property'])) {
                    $propType = $this->resolvePropertyType($rootClass, $filterDesc['property']);
                    if ($propType) {
                        $type = $propType;
                    }
                }

                /** @var string $alphanumName */
                $alphanumName = preg_replace('/\W+/', '_', $name);
                $propName = Inflector::camelize($alphanumName);

                $type = $this->resolveFilterType($filterType, $type);

                $type = $this->getSingularType($type);
                $propName = Inflector::singularize($propName);
                if (!empty($filterDesc['is_collection']) || substr($name, \strlen($name) - 2) === '[]') {
                    $type = new ArrayType($type);
                    $propName .= 'In';
                }

                $properties[] = new Property($propName, '', $type, false, true, false, $name);
            }
        }

        /** @var Property[] $propDict */
        $propDict = [];
        foreach ($properties as $property) {
            $name = $property->getName();
            if (isset($propDict[$name])) {
                if ($propDict[$name]->getOriginalName() !== $property->getOriginalName()) {
                    throw new InvalidArgumentException("filter property conflict: $name");
                }
                $mergedeType = Union::create([$propDict[$name]->getType(), $property->getType()]);
                $mergedNullable = $propDict[$name]->isNullable() || $property->isNullable();
                $propDict[$name] = new Property($name, '', $mergedeType, false, $mergedNullable, false, $property->getOriginalName());
            } else {
                $propDict[$name] = $property;
            }
        }

        return $propDict;
    }

    private function resolvePropertyType(string $class, string $property): ?Type
    {
        /** @var AnonymousObject|null $type */
        $type = $this->typeFactory->get($class)->findType(AnonymousObject::class);
        if (!$type || !$type->hasProperty($property)) {
            return null;
        }

        return $type->getProperty($property)->getType();
    }

    private function resolveFilterType(string $filterType, Type $baseType): Type
    {
        switch ($filterType) {
            case 'DateFilter':
                return BuiltinType::get('Date');

            case 'NumericFilter':
                return BuiltinType::get('number');

            case 'OrderFilter':
               return $this->typeFactory->get('Ordering');

            case 'BooleanFilter':
            case 'ExistFilter':
                return BuiltinType::get('boolean');
        }

        return $baseType;
    }

    private function getSingularType(Type $type): Type
    {
        if ($type instanceof AbstractCollection) {
            return $type->getValueType();
        }
        if ($type instanceof Reference) {
            return $this->getSingularType($type->getTarget());
        }

        return $type;
    }

    /**
     * @param Property[] $filterProperties
     */
    private function buildFilterBody(array $filterProperties, string $varName): array
    {
        $filterName = Inflector::classify(
            $this->operation->getOpDef()->getName()
            . $this->operation->getResource()->getBaseName()
            . 'Filters'
        );
        $filterType = new InterfaceType(
            $filterName,
            null,
            $filterProperties,
            sprintf(
                'Search filter(s) of %sRepository.%s',
                $this->operation->getResource()->getBaseName(),
                $this->operation->getName()
            )
        );
        $this->typeFactory->add($filterName, $filterType);

        $body = ["if (filters && typeof filters === 'object') {"];
        $body[] = "  if (!$varName) {";
        $body[] = "    $varName = {};";
        $body[] = '  }';
        foreach ($filterProperties as $property) {
            $assignation = sprintf(
                '  %s = %s;',
                $property->getUsage($varName, true),
                $property->getType()->castToStringOrStringArray($property->getUsage('filters'))
            );
            if ($property->isNullable()) {
                $body[] = sprintf(
                    "  if (%s) {\n  %s\n  }",
                    TypescriptHelper::propertyTestor('filters', $property->getName()),
                    $assignation
                );
            } else {
                $body[] = $assignation;
            }
        }
        $body[] = '}';

        return [$body, new Parameter('filters', $filterType, true)];
    }
}
