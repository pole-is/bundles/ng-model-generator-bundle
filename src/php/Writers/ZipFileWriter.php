<?php
declare(strict_types=1);
/*
 * This file is part of "irstea/ng-model-generator-bundle".
 *
 * "irstea/ng-model-generator-bundle" generates Typescript interfaces for Angular using api-platform metadata.
 * Copyright (C) 2018-2021 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\NgModelGeneratorBundle\Writers;

/**
 * Class ZipFileWriter.
 */
final class ZipFileWriter implements Writer
{
    /** @var \ZipArchive */
    private $archive;

    /** @var string */
    private $path;

    /** @var string[] */
    private $buffer = [];

    /**
     * ZipFileWriter constructor.
     */
    public function __construct(\ZipArchive $archive, string $path)
    {
        $this->archive = $archive;
        $this->path = $path;
    }

    /**
     * {@inheritdoc}
     */
    public function write(string $data): void
    {
        $this->buffer[] = $data;
    }

    public function close(): void
    {
        $this->archive->addFromString($this->path, implode('', $this->buffer));
        unset($this->archive, $this->path, $this->buffer);
    }
}
