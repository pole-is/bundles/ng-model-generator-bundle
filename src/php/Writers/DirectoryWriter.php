<?php
declare(strict_types=1);
/*
 * This file is part of "irstea/ng-model-generator-bundle".
 *
 * "irstea/ng-model-generator-bundle" generates Typescript interfaces for Angular using api-platform metadata.
 * Copyright (C) 2018-2021 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\NgModelGeneratorBundle\Writers;

use Assert\Assertion;

/**
 * Class DirectoryWriter.
 */
final class DirectoryWriter implements MultiFileWriter
{
    /** @var string */
    private $rootPath;

    /**
     * DirectoryWriter constructor.
     */
    public function __construct(string $rootPath)
    {
        $this->rootPath = $rootPath;
    }

    /**
     * {@inheritdoc}
     */
    public function newFile(string $path): Writer
    {
        $filePath = $this->rootPath . '/' . trim($path, '/');
        $fileDir = \dirname($filePath);

        /* @noinspection NotOptimalIfConditionsInspection */
        if (!is_dir($fileDir) && !mkdir($fileDir, 0777, true) && !is_dir($fileDir)) {
            throw new \RuntimeException(sprintf('Directory "%s" was not created', $fileDir));
        }

        $fh = fopen($filePath, 'w');
        Assertion::isResource($fh);

        return new StreamWriter($fh);
    }

    public function close(): void
    {
        // NOOP
    }
}
