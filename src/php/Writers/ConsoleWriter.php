<?php
declare(strict_types=1);
/*
 * This file is part of "irstea/ng-model-generator-bundle".
 *
 * "irstea/ng-model-generator-bundle" generates Typescript interfaces for Angular using api-platform metadata.
 * Copyright (C) 2018-2021 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\NgModelGeneratorBundle\Writers;

use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class ConsoleWriter.
 */
final class ConsoleWriter implements Writer, MultiFileWriter
{
    /** @var OutputInterface */
    private $output;

    /** @var string|null */
    private $currentFile = null;

    /**
     * ConsoleWriter constructor.
     */
    public function __construct(OutputInterface $output)
    {
        $this->output = $output;
    }

    /**
     * {@inheritdoc}
     */
    public function write(string $data): void
    {
        $this->output->write($data);
    }

    /**
     * {@inheritdoc}
     */
    public function newFile(string $path): Writer
    {
        $this->currentFile = $path;
        $this->output->write("===== Start of $path =====\n");

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function close(): void
    {
        if (isset($this->currentFile)) {
            $this->output->write("===== End of $this->currentFile =====\n");
            unset($this->currentFile);
        }
    }
}
