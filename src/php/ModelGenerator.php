<?php
declare(strict_types=1);
/*
 * This file is part of "irstea/ng-model-generator-bundle".
 *
 * "irstea/ng-model-generator-bundle" generates Typescript interfaces for Angular using api-platform metadata.
 * Copyright (C) 2018-2021 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\NgModelGeneratorBundle;

use ApiPlatform\Core\Documentation\Documentation;
use Irstea\NgModelGeneratorBundle\Exceptions\DomainException;
use Irstea\NgModelGeneratorBundle\Iterators\IteratorBuilder;
use Irstea\NgModelGeneratorBundle\Metadata\MetadataFactoryInterface;
use Irstea\NgModelGeneratorBundle\Metadata\ResourceMetadata;
use Irstea\NgModelGeneratorBundle\Models\ClassName;
use Irstea\NgModelGeneratorBundle\Models\Declaration;
use Irstea\NgModelGeneratorBundle\Models\PHPClass;
use Irstea\NgModelGeneratorBundle\Models\Types\Alias;
use Irstea\NgModelGeneratorBundle\Models\Types\BuiltinType;
use Irstea\NgModelGeneratorBundle\Models\Types\Objects\InterfaceType;
use Irstea\NgModelGeneratorBundle\Models\Types\Objects\Property;
use Irstea\NgModelGeneratorBundle\Models\Types\Objects\Repository;
use Irstea\NgModelGeneratorBundle\Models\Types\Resources\UUID;
use Irstea\NgModelGeneratorBundle\Models\Types\StringConst;
use Irstea\NgModelGeneratorBundle\Models\Types\Type;
use Irstea\NgModelGeneratorBundle\Models\Types\Union;
use Irstea\NgModelGeneratorBundle\Writers\MultiFileWriter;
use Symfony\Component\PropertyInfo\Type as PHPType;
use Twig\Environment;

/**
 * Class Serializer.
 */
final class ModelGenerator
{
    /** @var MetadataFactoryInterface */
    private $metadataFactory;

    /** @var Environment */
    private $twigEnv;

    /** @var Documentation */
    private $documentation;

    /** @var TypeFactoryInterface */
    private $typeFactory;

    /** @var SerializationMapperFactoryInterface */
    private $serializationMapperFactory;

    /**
     * Serializer constructor.
     */
    public function __construct(
        MetadataFactoryInterface $metadataFactory,
        Environment $twigEnv
    ) {
        $this->metadataFactory = $metadataFactory;
        $this->twigEnv = $twigEnv;
    }

    /**
     * Génère les modèles Typescript.
     * Cette méthode n'est pas réentrante.
     *
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    public function generate(Documentation $doc, MultiFileWriter $writer): void
    {
        if ($this->documentation !== null) {
            throw new DomainException(__METHOD__ . ' is not reentrant');
        }
        try {
            $this->documentation = $doc;

            $this->doGenerate($writer);
        } finally {
            unset(
                $this->documentation,
                $this->typeFactory
            );
        }
    }

    /**
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    private function doGenerate(MultiFileWriter $writer): void
    {
        $this->typeFactory = $this->createTypeFactory();

        $this->serializationMapperFactory = new SerializationMapperFactory($this->typeFactory);

        $context = [
            'title'        => $this->documentation->getTitle(),
            'version'      => $this->documentation->getVersion(),
            'description'  => $this->documentation->getDescription() ?: '',
            'repositories' => $this->extractRepositories(),
            'repoImports'  => [],
            'declarations' => [],
        ];

        $isResourceDeclaration = function ($item) {
            return $item instanceof Declaration
                && !($item instanceof Repository)
                && !\in_array($item->getName(), ['UUID', 'IRI', 'Collection', 'DateTime'], true);
        };

        foreach (
            IteratorBuilder::from($context['repositories'])
                ->unique()
                ->recurse(\RecursiveIteratorIterator::CHILD_FIRST)
                ->where($isResourceDeclaration)
            as $type) {
            $context['declarations'][$type->getName()] = $type;
        }

        foreach (
            IteratorBuilder::from($context['repositories'])
                ->unique()
                ->recurseWhere(
                    \RecursiveIteratorIterator::SELF_FIRST,
                    function ($item) {
                        return $item instanceof Repository
                            || !($item instanceof Declaration);
                    }
                )
                ->where($isResourceDeclaration)
            as $type) {
            $context['repoImports'][] = $type->getUsage();
        }
        sort($context['repoImports']);

        foreach (glob(__DIR__ . '/Resources/views/output/*.ts.twig') as $template) {
            $this->generateFile($writer, basename($template, '.twig'), $context);
        }
    }

    /**
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    private function generateFile(MultiFileWriter $writer, string $path, array $context): void
    {
        $fileWriter = $writer->newFile($path);

        try {
            $template = sprintf('@NgModelGenerator/output/%s.twig', basename($path));
            $context['filename'] = $path;
            $fileWriter->write($this->twigEnv->render($template, $context));
        } finally {
            $fileWriter->close();
        }
    }

    /**
     * Crée une usine à types contenant un certain nombre de types par défaut.
     */
    private function createTypeFactory(): TypeFactoryInterface
    {
        $factory = new TypeFactory();

        foreach (['Array', 'Date', 'boolean', 'number', 'null', 'string', 'object', 'any', 'never', 'unknown'] as $builtin) {
            $factory->add($builtin, BuiltinType::get($builtin));
        }

        $factory->add('UUID', UUID::get());
        $factory->add('CommonFilters', $this->createCommonFilters('CommonFilters'));
        $factory->add('Ordering', $this->createOrdering());

        foreach ([
            PHPType::BUILTIN_TYPE_ARRAY  => 'Array',
            PHPType::BUILTIN_TYPE_BOOL   => 'boolean',
            PHPType::BUILTIN_TYPE_OBJECT => 'object',
            PHPType::BUILTIN_TYPE_FLOAT  => 'number',
            PHPType::BUILTIN_TYPE_INT    => 'number',
            PHPType::BUILTIN_TYPE_NULL   => 'null',
            PHPType::BUILTIN_TYPE_STRING => 'string',
            'Ramsey\Uuid\UuidInterface'  => 'UUID',
            'DateTime'                   => 'string',
            'DateTimeInterface'          => 'DateTime',
            'DateTimeImmutable'          => 'DateTime',
        ] as $alias => $target) {
            if ($target === $alias || $factory->has($alias)) {
                continue;
            }
            $factory->add($alias, $factory->get($target));
        }

        return $factory;
    }

    private function createCommonFilters(string $name): Type
    {
        $properties = [];

        $meta = $this->metadataFactory->getPaginationMetadata();
        if ($meta->isEnabled()) {
            $properties[] = new Property($meta->getPageParameterName(), '', BuiltinType::get('number'), false, true);
            if ($meta->isClientItemsPerPage()) {
                $properties[] = new Property($meta->getItemsPerPageParameterName(), '', BuiltinType::get('number'), false, true);
            }
        }

        return new InterfaceType($name, null, $properties);
    }

    private function createOrdering(): Type
    {
        return new Alias('Ordering', Union::create([StringConst::get('asc'), StringConst::get('desc')]), 'Allowed values for ordering parameters');
    }

    /**
     * Retourne un iterateur sur les métadonnées des ressources.
     */
    private function getResourceMetadata(): \Iterator
    {
        $classNames = iterator_to_array($this->documentation->getResourceNameCollection()->getIterator());
        $classes = array_map([PHPClass::class, 'get'], $classNames);
        usort($classes, [PHPClass::class, 'baseNameOrdering']);

        foreach ($classes as $class) {
            yield $class => $this->metadataFactory->getResourceMetadata($class);
        }
    }

    /**
     * @return Repository[]
     */
    private function extractRepositories(): array
    {
        $repositories = [];

        // Premier passage pour prégénérer des référénces aux ressources
        foreach ($this->getResourceMetadata() as $class => $resourceMeta) {
            /* @var ClassName $class */
            /* @var ResourceMetadata $resourceMeta */
            $ref = $this->typeFactory->defer($class->getFullName());
            $this->typeFactory->add($resourceMeta->getBaseName(), $ref);
        }

        // Maintenant on génére les repositories
        foreach ($this->getResourceMetadata() as $class => $resourceMeta) {
            /* @var ClassName $class */
            /* @var ResourceMetadata $resourceMeta */
            try {
                $repo = $this->buildRepositories($resourceMeta);
                if ($repo) {
                    $repositories[$repo->getName()] = $repo;
                }
            } catch (\Throwable $ex) {
                throw new DomainException(sprintf('error with resource `%s`: %s', $resourceMeta->getBaseName(), $ex->getMessage()), 0, $ex);
            }
        }

        ksort($repositories);

        return $repositories;
    }

    private function buildRepositories(ResourceMetadata $resourceMeta): ?Repository
    {
        $defaultNormalization = $resourceMeta->getDefaultNormalization();

        $defaultNormalizationMapper = $this->serializationMapperFactory->create($defaultNormalization, true);

        /**
         * @var Type
         * @var Property   $identifier
         * @var Property[] $properties
         */
        [$defaultRepr, $identifier, $properties] = $defaultNormalizationMapper->getResourceData();

        if (!$properties) {
            printf(
                "No property found for %s (groups: [%s])\n",
                $resourceMeta->getBaseName(),
                implode(', ', $defaultNormalization->getGroups())
            );

            return null;
        }

        if (!$identifier) {
            printf(
                "No identifier found for %s (groups: [%s])\n",
                $resourceMeta->getBaseName(),
                implode(', ', $defaultNormalization->getGroups())
            );

            return null;
        }

        $operations = [];
        $pathParser = new PathParser($properties);

        $opsMeta = $resourceMeta->getOperations();
        if (!$opsMeta) {
            return null;
        }

        $iri = null;
        if (isset($opsMeta['getitem'])) {
            $get = $opsMeta['getitem'];
            $iri = $pathParser->parse($get->getPath(), $get->getRequirements());
        } else {
            foreach ($opsMeta as $operation) {
                if ($operation->isItemOperation()) {
                    $iri = $pathParser->parse($operation->getPath(), $operation->getRequirements());
                    break;
                }
            }
        }

        foreach ($opsMeta as $operation) {
            $mapper = new OperationMapper($this->typeFactory, $this->serializationMapperFactory, $pathParser, $operation, $iri);
            $operations[] = $mapper();
        }

        if (!$operations) {
            return null;
        }

        return new Repository($resourceMeta, $defaultRepr, $identifier, $iri, $operations);
    }
}
