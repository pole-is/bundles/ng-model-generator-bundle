<?php
declare(strict_types=1);
/*
 * This file is part of "irstea/ng-model-generator-bundle".
 *
 * "irstea/ng-model-generator-bundle" generates Typescript interfaces for Angular using api-platform metadata.
 * Copyright (C) 2018-2021 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\NgModelGeneratorBundle\Metadata;

use ApiPlatform\Core\Api\ResourceClassResolverInterface;
use ApiPlatform\Core\Exception\ResourceClassNotFoundException;
use ApiPlatform\Core\Metadata\Property\Factory\PropertyMetadataFactoryInterface;
use ApiPlatform\Core\Metadata\Property\Factory\PropertyNameCollectionFactoryInterface;
use ApiPlatform\Core\Metadata\Property\PropertyMetadata as APIPropertyMetadata;
use Irstea\NgModelGeneratorBundle\Models\ClassName;
use Symfony\Component\PropertyInfo\Type;

/**
 * Class PropertyMetadataFactory.
 */
class PropertyMetadataFactory
{
    public const MODE_CREATE = 'CREATE';
    public const MODE_UPDATE = 'UPDATE';
    public const MODE_READ = 'READ';
    public const MODE_OTHER = 'OTHER';

    /** @var PropertyNameCollectionFactoryInterface */
    private $propertyNameCollectionFactory;

    /** @var PropertyMetadataFactoryInterface */
    private $propertyMetadataFactory;

    /** @var ClassName */
    private $resource;

    /** @var string */
    private $mode;

    /** @var array */
    private $groups;

    /** @var array */
    private $properties = [];

    /** @var ResourceClassResolverInterface */
    private $resourceClassResolver;

    /**
     * PropertyMetadataFactory constructor.
     */
    public function __construct(
        ResourceClassResolverInterface $resourceClassResolver,
        PropertyNameCollectionFactoryInterface $propertyNameCollectionFactory,
        PropertyMetadataFactoryInterface $propertyMetadataFactory,
        ClassName $resource,
        string $mode,
        array $groups
    ) {
        $this->propertyNameCollectionFactory = $propertyNameCollectionFactory;
        $this->propertyMetadataFactory = $propertyMetadataFactory;
        $this->resource = $resource;
        $this->mode = $mode;
        $this->groups = $groups;
        $this->resourceClassResolver = $resourceClassResolver;
    }

    /**
     * @throws ResourceClassNotFoundException
     * @throws \ApiPlatform\Core\Exception\PropertyNotFoundException
     */
    public function create(ClassName $class, string $propertyName): PropertyMetadata
    {
        $propertyMeta = $this->getAPIMetadata($class)[$propertyName];

        $typeMeta = $propertyMeta->getType();
        \assert($typeMeta !== null);

        [$link, $embedded] = $this->getLinkStatus($propertyMeta);

        $nullable = $this->mode === self::MODE_UPDATE || $typeMeta->isNullable();

        return new PropertyMetadata(
            $propertyName,
            '',
            $typeMeta,
            $propertyMeta->isIdentifier() ?: false,
            $nullable,
            $propertyMeta->isReadable() ?: false,
            $propertyMeta->isWritable() ?: false,
            (bool) $propertyMeta->isInitializable(),
            $link,
            $embedded
        );
    }

    private function getLinkStatus(APIPropertyMetadata $propertyMeta): array
    {
        $typeMeta = $propertyMeta->getType();
        \assert($typeMeta !== null);

        $leafType = $this->getLeafType($typeMeta);
        if (!$leafType) {
            return [false, false];
        }

        $leafClassName = $leafType->getClassName();
        if (!$leafClassName || !class_exists($leafClassName)) {
            return [false, false];
        }

        if ($this->resourceClassResolver->isResourceClass($leafClassName)) {
            $embedded = $this->mode === self::MODE_READ ? $propertyMeta->isReadableLink() : $propertyMeta->isWritableLink();

            return [true, (bool) $embedded];
        }

        $reflection = new \ReflectionClass($leafClassName);

        return [false, $reflection->isUserDefined()];
    }

    private function getLeafType(Type $type): ?Type
    {
        while ($type && $type->isCollection()) {
            $type = $type->getCollectionValueType();
        }

        return $type;
    }

    /**
     * @throws ResourceClassNotFoundException
     * @throws \ApiPlatform\Core\Exception\PropertyNotFoundException
     *
     * @return APIPropertyMetadata[]
     */
    public function getAPIMetadata(ClassName $class): array
    {
        $key = $class->getFullName();
        if (!isset($this->properties[$key])) {
            $this->properties[$key] = $this->doGetAPIMetadata($class);
        }

        return $this->properties[$key];
    }

    /**
     * @throws \ApiPlatform\Core\Exception\PropertyNotFoundException
     * @throws ResourceClassNotFoundException
     *
     * @return APIPropertyMetadata[]
     */
    private function doGetAPIMetadata(ClassName $class): array
    {
        $properties = [];
        $options = $this->groups ? ['serializer_groups' => $this->groups] : [];
        $isResource = $class->getFullName() === $this->resource->getFullName();

        foreach ($this->propertyNameCollectionFactory->create($class->getFullName(), $options) as $propertyName) {
            \assert(\is_string($propertyName));

            $propertyMeta = $this->propertyMetadataFactory->create($class->getFullName(), $propertyName);

            if (!$propertyMeta->getType() || $propertyMeta->hasChildInherited()) {
                continue;
            }

            if (!$this->acceptProperty($isResource, $propertyMeta)) {
                continue;
            }

            $properties[$propertyName] = $propertyMeta;
        }

        return $properties;
    }

    private function acceptProperty(bool $isResource, APIPropertyMetadata $propertyMeta): bool
    {
        if (!$isResource && $propertyMeta->isIdentifier()) {
            return true;
        }

        switch ($this->mode) {
            case self::MODE_CREATE:
                return $propertyMeta->isWritable() || $propertyMeta->isInitializable();

            case self::MODE_READ:
                return $propertyMeta->isReadable() ?: false;

            case self::MODE_UPDATE:
                return $propertyMeta->isWritable() ?: false;

            default:
                return $propertyMeta->isReadable() || $propertyMeta->isWritable();
        }
    }
}
