<?php
declare(strict_types=1);
/*
 * This file is part of "irstea/ng-model-generator-bundle".
 *
 * "irstea/ng-model-generator-bundle" generates Typescript interfaces for Angular using api-platform metadata.
 * Copyright (C) 2018-2021 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\NgModelGeneratorBundle\Metadata;

use Irstea\NgModelGeneratorBundle\Models\ClassName;

/**
 * Class SerializationMetadata.
 */
final class SerializationMetadata implements ClassName
{
    /** @var ClassName */
    private $root;

    /** @var string[] */
    private $groups;

    /** @var bool */
    private $normalization;

    /** @var RepresentationMetadata[] */
    private $representations = [];

    /**
     * SerializationMetadata constructor.
     *
     * @param string[]                 $groups
     * @param RepresentationMetadata[] $representations
     */
    public function __construct(ClassName $root, array $groups, bool $normalization, array $representations)
    {
        $this->groups = $groups;
        sort($this->groups);

        $this->root = $root;
        $this->normalization = $normalization;

        foreach ($representations as $representation) {
            $this->representations[$representation->getFullName()] = $representation;
        }
        ksort($this->representations);
    }

    /**
     * Get groups.
     *
     * @return string[]
     */
    public function getGroups(): array
    {
        return $this->groups;
    }

    /**
     * Get root.
     */
    public function getRoot(): ClassName
    {
        return $this->root;
    }

    /**
     * {@inheritdoc}
     */
    public function getNamespace(): string
    {
        return $this->root->getNamespace();
    }

    /**
     * {@inheritdoc}
     */
    public function getBaseName(): string
    {
        return $this->root->getBaseName();
    }

    /**
     * {@inheritdoc}
     */
    public function getFullName(): string
    {
        return $this->root->getFullName();
    }

    /**
     * {@inheritdoc}
     */
    public function __toString()
    {
        return $this->root->__toString();
    }

    /**
     * Get representations.
     *
     * @return RepresentationMetadata[]
     */
    public function getRepresentations(): array
    {
        return $this->representations;
    }

    public function hasRepresentationOf(ClassName $class): bool
    {
        return isset($this->representations[$class->getFullName()]);
    }

    public function getRepresentationOf(ClassName $class): RepresentationMetadata
    {
        return $this->representations[$class->getFullName()];
    }

    /**
     * Get normalization.
     */
    public function isNormalization(): bool
    {
        return $this->normalization;
    }

    /**
     * @return ClassName[]
     */
    public function getRootSubClasses(): array
    {
        $subclasses = [];
        $rootClassName = $this->getFullName();
        foreach ($this->representations as $className => $meta) {
            if (is_subclass_of($className, $rootClassName, true)) {
                $subclasses[$className] = $meta;
            }
        }

        return $subclasses;
    }

    /**
     * {@inheritdoc}
     */
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}
