<?php
declare(strict_types=1);
/*
 * This file is part of "irstea/ng-model-generator-bundle".
 *
 * "irstea/ng-model-generator-bundle" generates Typescript interfaces for Angular using api-platform metadata.
 * Copyright (C) 2018-2021 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\NgModelGeneratorBundle\Metadata;

use ApiPlatform\Core\Api\FilterInterface;
use Irstea\NgModelGeneratorBundle\Models\HasName;

/**
 * Class OperationMetadata.
 */
class OperationMetadata implements \JsonSerializable, HasName
{
    /** @var OperationDef */
    private $opDef;

    /** @var string */
    private $description;

    /** @var string */
    private $path;

    /** @var FilterInterface[] */
    private $filters;

    /** @var ResourceMetadata */
    private $resource;

    /** @var string[] */
    private $requirements;

    /** @var PaginationMetadata|null */
    private $pagination;

    /** @var SerializationMetadata|null */
    private $normalization;

    /** @var SerializationMetadata|null */
    private $denormalization;

    /**
     * OperationMetadata constructor.
     */
    public function __construct(
        OperationDef $opDef,
        string $description,
        string $path,
        array $requirements,
        array $filters,
        ?PaginationMetadata $pagination,
        ?SerializationMetadata $normalization,
        ?SerializationMetadata $denormalization
    ) {
        $this->path = $path;
        $this->filters = $filters;
        $this->description = $description;
        $this->pagination = $pagination;
        $this->requirements = $requirements;
        $this->normalization = $normalization;
        $this->denormalization = $denormalization;
        $this->opDef = $opDef;
    }

    /**
     * @return OperationMetadata
     */
    public function withResource(ResourceMetadata $resource): self
    {
        $new = clone $this;
        $new->resource = $resource;

        return $new;
    }

    /**
     * Get className.
     */
    public function getClassName(): string
    {
        return $this->resource->getBaseName();
    }

    /**
     * Get opDef.
     */
    public function getOpDef(): OperationDef
    {
        return $this->opDef;
    }

    /**
     * Get name.
     */
    public function getName(): string
    {
        return $this->opDef->getName();
    }

    /**
     * Get description.
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * Get type.
     */
    public function getType(): string
    {
        return $this->opDef->isCollection() ? 'collection' : 'item';
    }

    public function isItemOperation(): bool
    {
        return !$this->opDef->isCollection();
    }

    public function isCollectionOperation(): bool
    {
        return $this->opDef->isCollection();
    }

    /**
     * Get method.
     */
    public function getMethod(): string
    {
        return $this->opDef->getMethod();
    }

    /**
     * Get path.
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * Get requirements.
     *
     * @return string[]
     */
    public function getRequirements(): array
    {
        return $this->requirements;
    }

    /**
     * Get pagination.
     */
    public function getPagination(): ?PaginationMetadata
    {
        return $this->pagination;
    }

    /**
     * Get filters.
     *
     * @return FilterInterface[]
     */
    public function getFilters(): array
    {
        return $this->filters;
    }

    /**
     * Get normalization.
     */
    public function getNormalization(): ?SerializationMetadata
    {
        return $this->normalization;
    }

    /**
     * Get denormalization.
     */
    public function getDenormalization(): ?SerializationMetadata
    {
        return $this->denormalization;
    }

    /**
     * Get resource.
     */
    public function getResource(): ResourceMetadata
    {
        return $this->resource;
    }

    /**
     * {@inheritdoc}
     */
    public function jsonSerialize()
    {
        $vars = array_merge(
            $this->opDef->jsonSerialize(),
            get_object_vars($this)
        );
        unset($vars['resource'], $vars['opDef']);

        return $vars;
    }
}
