<?php
declare(strict_types=1);
/*
 * This file is part of "irstea/ng-model-generator-bundle".
 *
 * "irstea/ng-model-generator-bundle" generates Typescript interfaces for Angular using api-platform metadata.
 * Copyright (C) 2018-2021 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\NgModelGeneratorBundle\Metadata;

use Irstea\NgModelGeneratorBundle\Exceptions\DomainException;
use Irstea\NgModelGeneratorBundle\Models\ClassName;
use Irstea\NgModelGeneratorBundle\Models\HasName;

/**
 * Class RepresentationMetadata.
 */
final class RepresentationMetadata implements ClassName, HasName
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var ClassName
     */
    private $class;

    /**
     * @var ClassName|null
     */
    private $parent;

    /**
     * @var array
     */
    private $properties = [];

    /**
     * @var bool
     */
    private $abstract;

    /** @var bool */
    private $resource;

    /**
     * RepresentationMetadata constructor.
     *
     * @param PropertyMetadata[] $properties
     */
    public function __construct(string $name, ClassName $class, ?ClassName $parent, array $properties, bool $abstract, bool $resource)
    {
        if ($parent && $class->getFullName() === $parent->getFullName()) {
            throw new DomainException("$class cannot be its own parent");
        }

        $this->class = $class;
        $this->parent = $parent;

        foreach ($properties as $property) {
            $this->properties[$property->getName()] = $property;
        }
        ksort($this->properties);

        $this->name = $name;
        $this->abstract = $abstract;
        $this->resource = $resource;
    }

    /**
     * Get name.
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * {@inheritdoc}
     */
    public function getNamespace(): string
    {
        return $this->class->getNamespace();
    }

    /**
     * {@inheritdoc}
     */
    public function getBaseName(): string
    {
        return $this->class->getBaseName();
    }

    /**
     * {@inheritdoc}
     */
    public function getFullName(): string
    {
        return $this->class->getFullName();
    }

    /**
     * {@inheritdoc}
     */
    public function __toString()
    {
        return $this->class->__toString();
    }

    /**
     * Get parent.
     */
    public function getParent(): ?ClassName
    {
        return $this->parent;
    }

    /**
     * Get properties.
     *
     * @return PropertyMetadata[]
     */
    public function getProperties(): array
    {
        return $this->properties;
    }

    /**
     * Get abstract.
     */
    public function isAbstract(): bool
    {
        return $this->abstract;
    }

    /**
     * Get resource.
     */
    public function isResource(): bool
    {
        return $this->resource;
    }

    /**
     * {@inheritdoc}
     */
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}
