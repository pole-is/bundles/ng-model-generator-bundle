<?php
declare(strict_types=1);
/*
 * This file is part of "irstea/ng-model-generator-bundle".
 *
 * "irstea/ng-model-generator-bundle" generates Typescript interfaces for Angular using api-platform metadata.
 * Copyright (C) 2018-2021 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\NgModelGeneratorBundle\Metadata;

use Doctrine\Common\Cache\ArrayCache;
use Doctrine\Common\Cache\Cache;
use Irstea\NgModelGeneratorBundle\Models\ClassName;

/**
 * Class CachingMetadataFactory.
 */
final class CachingMetadataFactory implements MetadataFactoryInterface
{
    /** @var MetadataFactoryInterface */
    private $inner;

    /** @var Cache */
    private $cache;

    /**
     * CachedMetadataFactory constructor.
     */
    public function __construct(MetadataFactoryInterface $inner, Cache $cache = null)
    {
        $this->inner = $inner;
        $this->cache = $cache ?: new ArrayCache();
    }

    /**
     * {@inheritdoc}
     */
    public function isResource(ClassName $class): bool
    {
        return $this->inner->isResource($class);
    }

    /**
     * {@inheritdoc}
     */
    public function getResourceMetadata(ClassName $class): ResourceMetadata
    {
        return $this->memoize(__METHOD__, $class->getFullName(), function () use ($class) {
            return $this->inner->getResourceMetadata($class);
        });
    }

    /**
     * @return mixed
     */
    private function memoize(string $namespace, string $key, callable $compute)
    {
        $id = $namespace . '@' . $key;
        if (!$this->cache->contains($id)) {
            $data = $compute($key);
            $this->cache->save($id, $data);

            return $data;
        }

        return $this->cache->fetch($id);
    }

    /**
     * {@inheritdoc}
     */
    public function getPaginationMetadata(): PaginationMetadata
    {
        return $this->inner->getPaginationMetadata();
    }
}
