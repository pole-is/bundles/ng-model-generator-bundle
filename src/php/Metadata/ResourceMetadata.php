<?php
declare(strict_types=1);
/*
 * This file is part of "irstea/ng-model-generator-bundle".
 *
 * "irstea/ng-model-generator-bundle" generates Typescript interfaces for Angular using api-platform metadata.
 * Copyright (C) 2018-2021 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\NgModelGeneratorBundle\Metadata;

use Irstea\NgModelGeneratorBundle\Models\ClassName;

/**
 * Class ResourceMetadata.
 */
class ResourceMetadata implements ClassName
{
    /** @var ClassName */
    private $class;

    /** @var ClassName|null */
    private $parentClass;

    /** @var string */
    private $description;

    /** @var bool */
    private $abstract;

    /** @var OperationMetadata[] */
    private $operations = [];

    /** @var SerializationMetadata */
    private $defaultNormalization;

    /**
     * ResourceMetadata constructor.
     *
     * @param OperationMetadata[] $operations
     */
    public function __construct(
        ClassName $class,
        ?ClassName $parentClass,
        string $description,
        bool $abstract,
        SerializationMetadata $defaultNormalization,
        array $operations
    ) {
        $this->class = $class;
        $this->parentClass = $parentClass;
        $this->abstract = $abstract;
        $this->description = $description;
        $this->defaultNormalization = $defaultNormalization;

        foreach ($operations as $operation) {
            $this->operations[$operation->getName() . $operation->getType()] = $operation->withResource($this);
        }
        ksort($this->operations);
    }

    /**
     * {@inheritdoc}
     */
    public function getNamespace(): string
    {
        return $this->class->getNamespace();
    }

    /**
     * {@inheritdoc}
     */
    public function getBaseName(): string
    {
        return $this->class->getBaseName();
    }

    /**
     * {@inheritdoc}
     */
    public function getFullName(): string
    {
        return $this->class->getFullName();
    }

    /**
     * {@inheritdoc}
     */
    public function __toString()
    {
        return $this->getFullName();
    }

    /**
     * Get parentClass.
     */
    public function getParentClass(): ?ClassName
    {
        return $this->parentClass;
    }

    /**
     * Get parentClassName.
     */
    public function getParentClassName(): ?string
    {
        return $this->parentClass ? $this->parentClass->getFullName() : null;
    }

    /**
     * Get description.
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * Get abstract.
     */
    public function isAbstract(): bool
    {
        return $this->abstract;
    }

    /**
     * Get operations.
     *
     * @return OperationMetadata[]
     */
    public function getOperations(): array
    {
        return $this->operations;
    }

    /**
     * Get defaultNormalization.
     */
    public function getDefaultNormalization(): SerializationMetadata
    {
        return $this->defaultNormalization;
    }

    /**
     * {@inheritdoc}
     */
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}
