<?php
declare(strict_types=1);
/*
 * This file is part of "irstea/ng-model-generator-bundle".
 *
 * "irstea/ng-model-generator-bundle" generates Typescript interfaces for Angular using api-platform metadata.
 * Copyright (C) 2018-2021 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\NgModelGeneratorBundle\Metadata;

use ApiPlatform\Core\Metadata\Resource\ResourceNameCollection;
use Irstea\NgModelGeneratorBundle\Models\ClassName;
use Irstea\NgModelGeneratorBundle\Models\PHPClass;

/**
 * Class ResourceClassHierarchy.
 */
final class ResourceClassHierarchy implements ClassHierarchy
{
    /** @var ClassName[] */
    private $parents = [];

    /** @var ClassName[][] */
    private $children = [];

    /**
     * ResourceClassHierarchy constructor.
     */
    public function __construct(ResourceNameCollection $nameCollection)
    {
        foreach ($nameCollection->getIterator() as $className) {
            $this->preload(PHPClass::get($className));
        }
    }

    private function preload(ClassName $class): void
    {
        $className = $class->getFullName();
        if (\array_key_exists($className, $this->parents)) {
            return;
        }

        $reflClass = new \ReflectionClass($className);
        $reflParent = $reflClass->getParentClass();
        if (!$reflParent) {
            unset($this->parents[$className]);

            return;
        }
        $parent = PHPClass::get($reflParent);

        $this->parents[$className] = $parent;

        $parentClassName = $parent->getFullName();
        if (!isset($this->children[$parentClassName])) {
            $this->children[$parentClassName] = [];
        }
        $this->children[$parentClassName][$className] = $class;
    }

    /**
     * {@inheritdoc}
     */
    public function getParent(ClassName $class): ?ClassName
    {
        $this->preload($class);

        return $this->parents[$class->getFullName()] ?? null;
    }

    /**
     * {@inheritdoc}
     */
    public function getChildren(ClassName $class): array
    {
        $this->preload($class);

        return $this->children[$class->getFullName()] ?? [];
    }
}
