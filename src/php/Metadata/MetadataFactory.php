<?php
declare(strict_types=1);
/*
 * This file is part of "irstea/ng-model-generator-bundle".
 *
 * "irstea/ng-model-generator-bundle" generates Typescript interfaces for Angular using api-platform metadata.
 * Copyright (C) 2018-2021 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\NgModelGeneratorBundle\Metadata;

use ApiPlatform\Core\Api\FilterInterface;
use ApiPlatform\Core\Api\OperationMethodResolverInterface;
use ApiPlatform\Core\Api\OperationType;
use ApiPlatform\Core\Api\ResourceClassResolverInterface;
use ApiPlatform\Core\Exception\PropertyNotFoundException;
use ApiPlatform\Core\Exception\ResourceClassNotFoundException;
use ApiPlatform\Core\Metadata\Property\Factory\PropertyMetadataFactoryInterface;
use ApiPlatform\Core\Metadata\Property\Factory\PropertyNameCollectionFactoryInterface;
use ApiPlatform\Core\Metadata\Resource\Factory\ResourceMetadataFactoryInterface;
use ApiPlatform\Core\Metadata\Resource\ResourceMetadata as APIResourceMetadata;
use Assert\Assertion;
use Doctrine\Common\Inflector\Inflector;
use Irstea\NgModelGeneratorBundle\Models\ClassName;
use Irstea\NgModelGeneratorBundle\Models\PHPClass;
use Psr\Container\ContainerInterface;
use Symfony\Component\PropertyInfo\PropertyInfoExtractorInterface;
use Symfony\Component\Routing\Exception\RouteNotFoundException;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class MetadataFactory
 * Cette classe est chargée de collecter les métadonnées des différents services d'API-Platform.
 */
/* final */ class MetadataFactory implements MetadataFactoryInterface
{
    /** @var ResourceClassResolverInterface */
    private $resourceClassResolver;

    /** @var ResourceMetadataFactoryInterface */
    private $resourceMetadataFactory;

    /** @var PropertyNameCollectionFactoryInterface */
    private $propertyNameCollectionFactory;

    /** @var PropertyMetadataFactoryInterface */
    private $propertyMetadataFactory;

    /** @var PropertyInfoExtractorInterface */
    private $propertyInfoExtractor;

    /** @var OperationMethodResolverInterface */
    private $operationMethodResolver;

    /** @var ContainerInterface */
    private $filterLocator;

    /** @var PaginationMetadata */
    private $paginationMetadata;

    /** @var SerializationMetadata[] */
    private $serializations = [];

    /** @var ClassHierarchy */
    private $classHierarchy;

    /** @var string[][] */
    private $defaultGroups = [];

    /** @var RouterInterface */
    private $router;

    /**
     * MetadataFactory constructor.
     */
    public function __construct(
        ResourceClassResolverInterface $resourceClassResolver,
        ResourceMetadataFactoryInterface $resourceMetadataFactory,
        PropertyNameCollectionFactoryInterface $propertyNameCollectionFactory,
        PropertyMetadataFactoryInterface $propertyMetadataFactory,
        PropertyInfoExtractorInterface $propertyInfoExtractor,
        OperationMethodResolverInterface $operationMethodResolver,
        RouterInterface $router,
        ContainerInterface $filterLocator,
        PaginationMetadata $paginationMetadata,
        ClassHierarchy $classHierarchy
    ) {
        $this->resourceClassResolver = $resourceClassResolver;
        $this->resourceMetadataFactory = $resourceMetadataFactory;
        $this->propertyNameCollectionFactory = $propertyNameCollectionFactory;
        $this->propertyMetadataFactory = $propertyMetadataFactory;
        $this->propertyInfoExtractor = $propertyInfoExtractor;
        $this->operationMethodResolver = $operationMethodResolver;
        $this->filterLocator = $filterLocator;
        $this->paginationMetadata = $paginationMetadata;
        $this->classHierarchy = $classHierarchy;
        $this->router = $router;
    }

    /**
     * {@inheritdoc}
     */
    public function isResource(ClassName $class): bool
    {
        return $this->resourceClassResolver->isResourceClass($class->getFullName());
    }

    /**
     * {@inheritdoc}
     */
    public function getResourceMetadata(ClassName $class): ResourceMetadata
    {
        $className = $class->getFullName();
        $metadata = $this->resourceMetadataFactory->create($className);

        $classMeta = new \ReflectionClass($className);
        $parentClass = $classMeta->getParentClass();

        $defaultNormalization = $this->getOperationSerialization(
            null,
            $class,
            true,
            $metadata->getAttribute('normalization_context', [])['groups'] ?? []
        );

        return new ResourceMetadata(
            $class,
            $parentClass ? PHPClass::get($parentClass->getName()) : null,
            $metadata->getDescription() ?: '',
            $classMeta->isAbstract(),
            $defaultNormalization,
            $this->getOperations($class)
        );
    }

    private function buildPagination(bool $enabled, bool $clientItemsPerPage): PaginationMetadata
    {
        return new PaginationMetadata(
            $enabled && $this->paginationMetadata->isEnabled(),
            $this->paginationMetadata->getPageParameterName(),
            $clientItemsPerPage && $this->paginationMetadata->isClientItemsPerPage(),
            $this->paginationMetadata->getItemsPerPageParameterName()
        );
    }

    /**
     * Get paginationMetadata.
     */
    public function getPaginationMetadata(): PaginationMetadata
    {
        return $this->paginationMetadata;
    }

    /**
     * @throws PropertyNotFoundException
     * @throws ResourceClassNotFoundException
     * @throws \ReflectionException
     *
     * @return OperationMetadata[]
     */
    private function getOperations(ClassName $class): array
    {
        $resourceMetadata = $this->resourceMetadataFactory->create($class->getFullName());

        $operations = [];

        foreach ([
            OperationType::ITEM       => $resourceMetadata->getItemOperations(),
            OperationType::COLLECTION => $resourceMetadata->getCollectionOperations(),
        ] as $type => $ops) {
            if (!$ops) {
                continue;
            }
            foreach ($ops as $name => $operation) {
                $operations[] = $this->getOperation($class, $resourceMetadata, (string) $name, $type, $operation);
            }
        }

        return $operations;
    }

    /**
     * @throws PropertyNotFoundException
     * @throws ResourceClassNotFoundException
     * @throws \ReflectionException
     */
    private function getOperation(
        ClassName $class,
        APIResourceMetadata $resourceMetadata,
        string $name,
        string $type,
        array $operation
    ): OperationMetadata {
        if ($type === OperationType::ITEM) {
            $method = $this->operationMethodResolver->getItemOperationMethod($class->getFullName(), $name);
        } else {
            $method = $this->operationMethodResolver->getCollectionOperationMethod($class->getFullName(), $name);
        }

        $shortName = $resourceMetadata->getShortName();
        Assertion::notNull($shortName);

        $path = $this->getOperationPath($class, $type, $name, $method);

        $getAttribute = function (string $attrName, $default) use ($resourceMetadata, $type, $name) {
            return $resourceMetadata->getTypedOperationAttribute($type, $name, $attrName, $default, true);
        };

        if ($type === OperationType::COLLECTION && $method === 'GET') {
            $filters = $this->getFilters($class, $getAttribute('filters', []));

            $paginationEnabled = (bool) $getAttribute('pagination_enabled', true);
            $paginationClientItemsPerPage = (bool) $getAttribute('pagination_client_items_per_page', true);

            $pagination = $this->buildPagination($paginationEnabled, $paginationClientItemsPerPage);
        } else {
            $filters = [];
            $pagination = null;
        }

        $opDef = new OperationDef($name, $method, $type === OperationType::COLLECTION);

        $output = $getAttribute('output', null);
        $normalization = null;
        if ($output !== false) {
            if (\is_array($output) && \array_key_exists('class', $output)) {
                $output = $output['class'];
            } elseif ($output === null && $opDef->hasNormalization()) {
                $output = $class->getFullName();
            }
            if ($output) {
                $normalization = $this->getOperationSerialization(
                    $opDef,
                    PHPClass::get($output),
                    true,
                    $getAttribute('normalization_context', [])['groups'] ?? []
                );
            }
        }

        $input = $getAttribute('input', null);
        $denormalization = null;
        if ($input !== false) {
            if (\is_array($input) && \array_key_exists('class', $input)) {
                $input = $input['class'];
            } elseif ($input === null && $opDef->hasDenormalization()) {
                $input = $class->getFullName();
            }
            if ($input) {
                $denormalization = $this->getOperationSerialization(
                    $opDef,
                    PHPClass::get($input),
                    false,
                    $getAttribute('denormalization_context', [])['groups'] ?? []
                );
            }
        }

        return new OperationMetadata(
            $opDef,
            $operation['description'] ?? '',
            $path,
            $getAttribute('requirements', []),
            $filters,
            $pagination,
            $normalization,
            $denormalization
        );
    }

    private function getOperationPath(ClassName $class, string $type, string $name, string $method): string
    {
        $className = $class->getFullName();
        $path = null;

        foreach ($this->router->getRouteCollection() as $route) {
            if (
                 $route->getDefault('_api_resource_class') === $className
                 && $route->getDefault("_api_${type}_operation_name") === $name
                 && \in_array($method, $route->getMethods(), true)) {
                $path = $route->getPath();
                break;
            }
        }

        if (!$path) {
            throw new RouteNotFoundException("No route found for ${$type} operation ${name} on ${className}");
        }

        $path = str_replace('.{_format}', '', $path);

        return $path;
    }

    /**
     * @return FilterInterface[]
     */
    private function getFilters(ClassName $class, array $filterIds): array
    {
        $filters = [];

        foreach ($filterIds as $filterId) {
            if (!$this->filterLocator->has($filterId)) {
                continue;
            }

            $filters[] = $this->filterLocator->get($filterId);
        }

        return $filters;
    }

    /**
     * @parma string $operationName
     *
     * @param string[] $groups
     *
     * @throws PropertyNotFoundException
     * @throws ResourceClassNotFoundException
     * @throws \ReflectionException
     */
    private function getOperationSerialization(
        ?OperationDef $opDef,
        ClassName $class,
        bool $normalization,
        array $groups
    ): SerializationMetadata {
        sort($groups);
        $key =
            sprintf('%s:%d:%s:%s', $class->getFullName(), $normalization, $opDef ? $opDef->getName() : '_Default_', implode('+', $groups));
        if (!isset($this->serializations[$key])) {
            $this->serializations[$key] = $this->doGetSerialization($class, $normalization, $opDef, $groups);
        }

        return $this->serializations[$key];
    }

    /**
     * @param OperationDef $opDef
     * @param string[]     $groups
     *
     * @throws PropertyNotFoundException
     * @throws ResourceClassNotFoundException
     * @throws \ReflectionException
     */
    private function doGetSerialization(ClassName $class, bool $normalization, ?OperationDef $opDef, array $groups): SerializationMetadata
    {
        if ($normalization) {
            $mode = PropertyMetadataFactory::MODE_READ;
        } elseif ($opDef && $opDef->isCreateItem()) {
            $mode = PropertyMetadataFactory::MODE_CREATE;
        } elseif ($opDef && $opDef->isUpdateItem()) {
            $mode = PropertyMetadataFactory::MODE_UPDATE;
        } else {
            $mode = PropertyMetadataFactory::MODE_OTHER;
        }
        $propertyMetadataFactory = new PropertyMetadataFactory(
            $this->resourceClassResolver,
            $this->propertyNameCollectionFactory,
            $this->propertyMetadataFactory,
            $class,
            $mode,
            $groups
        );

        /** @var RepresentationMetadata[] $reprs */
        $representations = [];

        /** @var ClassName[] $queue */
        $queue = [$class];

        while ($queue) {
            $current = array_shift($queue);

            if (isset($representations[$current->getFullName()])) {
                continue;
            }

            $parent = $this->classHierarchy->getParent($current);
            if ($parent) {
                $queue[] = $parent;
            }

            foreach ($this->classHierarchy->getChildren($current) as $children) {
                $queue[] = $children;
            }

            $propertiesMeta = $propertyMetadataFactory->getAPIMetadata($current);

            $properties = [];
            foreach ($propertiesMeta as $propertyName => $propertyMeta) {
                $property = $propertyMetadataFactory->create($current, $propertyName);
                $properties[$propertyName] = $property;
                if ($property->isEmbedded()) {
                    $type = $property->getLeafType();
                    $className = $type->getClassName();
                    if ($className) {
                        $queue[] = PHPClass::get($className);
                    }
                }
            }

            $name = $this->getRepresentationName($class, $current, $normalization, $opDef, $groups);

            $abstract = (new \ReflectionClass($current->getFullName()))->isAbstract();

            $representations[$current->getFullName()] = new RepresentationMetadata($name, $current, $parent, $properties, $abstract, $this->isResource($current));
        }

        return new SerializationMetadata($class, $groups, $normalization, $representations);
    }

    /**
     * @throws ResourceClassNotFoundException
     */
    private function getRepresentationName(
        ClassName $root,
        ClassName $class,
        bool $normalization,
        ?OperationDef $opDef,
        array $groups
    ): string {
        if ($normalization && (!$groups || $groups === $this->getDefaultGroups($class))) {
            return $class->getBaseName();
        }

        if ($opDef) {
            $name = $opDef->getOriginalName();
        } elseif ($groups) {
            $name = implode('', $groups);
        } else {
            $name = $normalization ? 'Read' : 'Write';
        }

        if (strpos($name, $root->getBaseName()) === false) {
            $name .= $root->getBaseName();
        }

        if (strpos($name, $class->getBaseName()) === false) {
            $name .= $class->getBaseName();
        }

        return Inflector::classify($name);
    }

    /**
     * @throws ResourceClassNotFoundException
     */
    private function getDefaultGroups(ClassName $class): array
    {
        $className = $class->getFullName();
        if (isset($this->defaultGroups[$className])) {
            return $this->defaultGroups[$className];
        }

        $context = $this->resourceMetadataFactory->create($className)->getAttribute('normalization_context', []);
        $groups = $context['groups'] ?? [];

        sort($groups);
        $this->defaultGroups[$className] = $groups;

        return $groups;
    }
}
