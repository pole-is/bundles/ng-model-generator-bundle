<?php
declare(strict_types=1);
/*
 * This file is part of "irstea/ng-model-generator-bundle".
 *
 * "irstea/ng-model-generator-bundle" generates Typescript interfaces for Angular using api-platform metadata.
 * Copyright (C) 2018-2021 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\NgModelGeneratorBundle\Metadata;

/**
 * Class PaginationMetadata.
 */
final class PaginationMetadata implements \JsonSerializable
{
    /**
     * @var bool
     */
    private $enabled;

    /*** @var string */
    private $pageParameterName;

    /** @var bool */
    private $clientItemsPerPage;

    /** @var string */
    private $itemsPerPageParameterName;

    /**
     * PaginationMetadata constructor.
     */
    public function __construct(bool $enabled, string $pageParameterName, bool $clientItemsPerPage, string $itemsPerPageParameterName)
    {
        $this->enabled = $enabled;
        $this->pageParameterName = $pageParameterName;
        $this->clientItemsPerPage = $clientItemsPerPage;
        $this->itemsPerPageParameterName = $itemsPerPageParameterName;
    }

    /**
     * Get enabled.
     */
    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    /**
     * Get pageParameterName.
     */
    public function getPageParameterName(): string
    {
        return $this->pageParameterName;
    }

    /**
     * Get clientItemsPerPage.
     */
    public function isClientItemsPerPage(): bool
    {
        return $this->clientItemsPerPage;
    }

    /**
     * Get itemsPerPageParameterName.
     */
    public function getItemsPerPageParameterName(): string
    {
        return $this->itemsPerPageParameterName;
    }

    /**
     * {@inheritdoc}
     */
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}
