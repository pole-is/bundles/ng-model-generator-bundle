<?php
declare(strict_types=1);
/*
 * This file is part of "irstea/ng-model-generator-bundle".
 *
 * "irstea/ng-model-generator-bundle" generates Typescript interfaces for Angular using api-platform metadata.
 * Copyright (C) 2018-2021 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\NgModelGeneratorBundle\Metadata;

use Doctrine\Common\Inflector\Inflector;
use Irstea\NgModelGeneratorBundle\Models\HasName;

/**
 * Class OperationDef.
 */
final class OperationDef implements \JsonSerializable, HasName
{
    /** @var string */
    private $name;

    /** @var string */
    private $originalName;

    /** @var string */
    private $method;

    /** @var bool */
    private $isCollection;

    /** @var string|null */
    private $special;

    /**
     * OperationDef constructor.
     */
    public function __construct(string $name, string $method, bool $isCollection)
    {
        $this->originalName = $name;
        $this->method = $method;

        if (strtolower($method) === strtolower($name)) {
            $this->special = strtoupper($method);
            if ($method === 'POST') {
                $isCollection = false;
            }
        }
        $this->isCollection = $isCollection;

        $name = Inflector::camelize($name);

        if ($method === 'GET' && strpos($name, 'get') === false) {
            $name = 'get' . ucfirst($name);
        }

        if ($isCollection) {
            if (\in_array($name, ['get', 'put', 'delete', 'patch'], true)) {
                $name .= 'All';
            } else {
                $name = Inflector::pluralize($name);
            }
        } else {
            $name = Inflector::singularize($name);
        }

        $this->name = $name;
    }

    public function hasNormalization(): bool
    {
        return \in_array($this->method, ['GET', 'PUT', 'POST']);
    }

    public function hasDenormalization(): bool
    {
        return \in_array($this->method, ['PUT', 'POST']);
    }

    public function isGetItem(): bool
    {
        return $this->special === 'GET' && !$this->isCollection;
    }

    public function isCreateItem(): bool
    {
        return $this->special === 'POST' && !$this->isCollection;
    }

    public function isUpdateItem(): bool
    {
        return $this->special === 'PUT' && !$this->isCollection;
    }

    public function isDeleteItem(): bool
    {
        return $this->special === 'DELETE' && !$this->isCollection;
    }

    public function isGetCollection(): bool
    {
        return $this->method === 'GET' && $this->isCollection;
    }

    /**
     * Get name.
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Get originalName.
     */
    public function getOriginalName(): string
    {
        return $this->originalName;
    }

    /**
     * Get method.
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * Get isCollection.
     */
    public function isCollection(): bool
    {
        return $this->isCollection;
    }

    /**
     * {@inheritdoc}
     */
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}
