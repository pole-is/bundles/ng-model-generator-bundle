<?php
declare(strict_types=1);
/*
 * This file is part of "irstea/ng-model-generator-bundle".
 *
 * "irstea/ng-model-generator-bundle" generates Typescript interfaces for Angular using api-platform metadata.
 * Copyright (C) 2018-2021 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\NgModelGeneratorBundle\Metadata;

use Assert\Assertion;
use Irstea\NgModelGeneratorBundle\Models\HasName;
use Symfony\Component\PropertyInfo\Type;

/**
 * Class PropertyMetadata.
 */
class PropertyMetadata implements \JsonSerializable, HasName
{
    /** @var string */
    private $name;

    /** @var string */
    private $description;

    /** @var Type */
    private $type;

    /** @var bool */
    private $identifier;

    /** @var bool */
    private $readable;

    /** @var bool */
    private $writable;

    /** @var bool */
    private $initializable;

    /** @var bool */
    private $link;

    /** @var bool */
    private $embedded;

    /** @var bool */
    private $nullable;

    /**
     * PropertyMetadata constructor.
     */
    public function __construct(
        string $name,
        string $description,
        Type $type,
        bool $identifier,
        bool $nullable,
        bool $readable,
        bool $writable,
        bool $initializable,
        bool $link,
        bool $embedded
    ) {
        $this->name = $name;
        $this->description = $description;
        $this->type = $type;
        $this->identifier = $identifier;
        $this->nullable = $nullable;
        $this->readable = $readable;
        $this->writable = $writable;
        $this->initializable = $initializable;
        $this->link = $link;
        $this->embedded = $embedded;
    }

    /**
     * Get name.
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Get description.
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * Get type.
     */
    public function getType(): Type
    {
        return $this->type;
    }

    public function getLeafType(): Type
    {
        $type = $this->type;
        while ($type->isCollection() && $type->getCollectionValueType()) {
            $type = $type->getCollectionValueType();
            Assertion::notNull($type);
        }

        return $type;
    }

    /**
     * Get identifier.
     */
    public function isIdentifier(): bool
    {
        return $this->identifier;
    }

    /**
     * Get readable.
     */
    public function isReadable(): bool
    {
        return $this->readable;
    }

    /**
     * Get writable.
     */
    public function isWritable(): bool
    {
        return $this->writable;
    }

    public function isNullable(): bool
    {
        return $this->nullable;
    }

    /**
     * Get initializable.
     */
    public function isInitializable(): bool
    {
        return $this->initializable;
    }

    /**
     * Get link.
     */
    public function isLink(): bool
    {
        return $this->link;
    }

    /**
     * Get embedded.
     */
    public function isEmbedded(): bool
    {
        return $this->embedded;
    }

    /**
     * {@inheritdoc}
     */
    public function jsonSerialize()
    {
        $vars = get_object_vars($this);
        $vars['type'] = $this->serializeType($this->type);
        unset($vars['resource']);

        return $vars;
    }

    /**
     * @return array|null
     */
    private function serializeType(?Type $type)
    {
        if (!$type) {
            return null;
        }
        if ($type->getCollectionValueType()) {
            return [
                'key_type'   => $this->serializeType($type->getCollectionKeyType()),
                'value_type' => $this->serializeType($type->getCollectionValueType()),
                'nullable'   => $type->isNullable(),
            ];
        }
        if ($type->getClassName()) {
            return [
                'class_name' => $type->getClassName(),
                'nullable'   => $type->isNullable(),
            ];
        }

        return [
            'builtin_type' => $type->getBuiltinType(),
            'nullable'     => $type->isNullable(),
        ];
    }
}
