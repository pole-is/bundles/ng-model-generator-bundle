<?php
declare(strict_types=1);
/*
 * This file is part of "irstea/ng-model-generator-bundle".
 *
 * "irstea/ng-model-generator-bundle" generates Typescript interfaces for Angular using api-platform metadata.
 * Copyright (C) 2018-2021 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\NgModelGeneratorBundle;

/**
 * Class TypescriptHelper.
 */
final class TypescriptHelper
{
    /**
     * Détermine si une chaîne est un identifiant valide.
     */
    public static function isValidIdentifier(string $string): bool
    {
        return (bool) preg_match('/^[a-z_][a-z0-9_]*$/i', $string);
    }

    /**
     * Retourne une chaîne avec les échappements nécessaires.
     */
    public static function quoteString(string $string, string $delimiter = "'"): string
    {
        if (strpos($string, $delimiter) !== false) {
            if ($delimiter === "'" && strpos($string, '"') === false) {
                $delimiter = '"';
            } elseif ($delimiter === '"' && strpos($string, "'") === false) {
                $delimiter = "'";
            } else {
                $string = str_replace($delimiter, '\\' . $delimiter, $string);
            }
        }

        return $delimiter . $string . $delimiter;
    }

    /**
     * Retourne une chaîne pour déclarer une propriété dans un objet litéral.
     */
    public static function objectLiteralKey(string $name): string
    {
        return self::isValidIdentifier($name) ? $name : self::quoteString($name);
    }

    /**
     * Retourne une expression permettant de tester la présence d'une propriété dans un objet.
     */
    public static function propertyTestor(string $object, string $name): string
    {
        return sprintf('hasProperty(%s, %s)', $object, self::quoteString($name));
    }

    /**
     * Retourne une expression permettant d'accéder à une propriété dans un objet.
     */
    public static function propertyAccessor(string $object, string $name): string
    {
        if (self::isValidIdentifier($name)) {
            return sprintf('%s.%s', $object, $name);
        }

        return sprintf('%s[%s]', $object, self::quoteString($name));
    }

    /**
     * Indente un bloc de code.
     */
    public static function indent(string $code, string $indentation = '  '): string
    {
        return $indentation . implode("\n{$indentation}", explode("\n", trim($code)));
    }

    private function __construct()
    {
    }
}
