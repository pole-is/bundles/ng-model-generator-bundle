<?php
declare(strict_types=1);
/*
 * This file is part of "irstea/ng-model-generator-bundle".
 *
 * "irstea/ng-model-generator-bundle" generates Typescript interfaces for Angular using api-platform metadata.
 * Copyright (C) 2018-2021 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\NgModelGeneratorBundle\Command;

use ApiPlatform\Core\Documentation\Documentation;
use Assert\Assertion;
use Irstea\NgModelGeneratorBundle\Metadata\MetadataFactoryInterface;
use Irstea\NgModelGeneratorBundle\Models\PHPClass;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class NgModelMetadataCommand.
 */
/* final */ class NgModelMetadataCommand extends Command
{
    /**
     * @var MetadataFactoryInterface
     */
    private $metadataFactory;

    /**
     * @var Documentation
     */
    private $documentation;

    /**
     * NgModelGenerateCommand constructor.
     */
    public function __construct(
        MetadataFactoryInterface $metadataFactory,
        Documentation $documentation
    ) {
        parent::__construct();

        $this->metadataFactory = $metadataFactory;
        $this->documentation = $documentation;
    }

    /**
     * {@inheritdoc}
     */
    protected function configure(): void
    {
        $this
            ->setName('ng-model:metadata')
            ->setDescription('Dump model metadata')
            ->addArgument('classes', InputArgument::OPTIONAL | InputArgument::IS_ARRAY, 'Restrict dump to the listed classes.');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output): void
    {
        $classes = $input->getArgument('classes');
        Assertion::isArray($classes);
        Assertion::allString($classes);

        $metadata = [];
        foreach ($this->documentation->getResourceNameCollection() as $className) {
            $class = PHPClass::get($className);
            if ($classes && !\in_array($className, $classes, true) && !\in_array($class->getBaseName(), $classes, true)) {
                continue;
            }
            $metadata[$class->getBaseName()] = $this->metadataFactory->getResourceMetadata($class);
        }

        $json = json_encode($metadata, \JSON_PRETTY_PRINT | \JSON_UNESCAPED_SLASHES | \JSON_UNESCAPED_UNICODE);
        Assertion::string($json);
        $output->writeln($json);
    }
}
