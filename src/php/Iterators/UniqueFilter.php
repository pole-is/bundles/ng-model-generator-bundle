<?php
declare(strict_types=1);
/*
 * This file is part of "irstea/ng-model-generator-bundle".
 *
 * "irstea/ng-model-generator-bundle" generates Typescript interfaces for Angular using api-platform metadata.
 * Copyright (C) 2018-2021 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\NgModelGeneratorBundle\Iterators;

/**
 * Class UniqueFilter.
 */
final class UniqueFilter
{
    /**
     * @var array<string,bool>
     */
    private $seen = [];

    /**
     * @param mixed $value
     */
    public function __invoke($value): bool
    {
        $key = $this->getKey($value);
        $alreadySeen = isset($this->seen[$key]);
        $this->seen[$key] = true;

        return !$alreadySeen;
    }

    /**
     * @param mixed $value
     */
    private function getKey($value): string
    {
        if (\is_object($value)) {
            return \get_class($value) . '#' . spl_object_hash($value);
        }
        if (\is_array($value)) {
            return 'array#' . implode(', ', array_map(
                function ($value) { return $this->getKey($value); },
                $value
            ));
        }

        return \gettype($value) . '#' . (string) $value;
    }

    public function reset(): void
    {
        $this->seen = [];
    }
}
