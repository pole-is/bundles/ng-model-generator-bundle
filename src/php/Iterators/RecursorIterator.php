<?php
declare(strict_types=1);
/*
 * This file is part of "irstea/ng-model-generator-bundle".
 *
 * "irstea/ng-model-generator-bundle" generates Typescript interfaces for Angular using api-platform metadata.
 * Copyright (C) 2018-2021 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\NgModelGeneratorBundle\Iterators;

/**
 * Class RecursorIterator.
 */
class RecursorIterator extends \IteratorIterator implements \RecursiveIterator
{
    /**
     * {@inheritdoc}
     */
    public function hasChildren()
    {
        $value = $this->current();

        return \is_array($value) || $value instanceof \Iterator || $value instanceof \IteratorAggregate;
    }

    /**
     * {@inheritdoc}
     */
    public function getChildren()
    {
        $value = $this->current();
        if (\is_array($value)) {
            $iter = new \ArrayIterator($value);
        } elseif ($value instanceof \IteratorAggregate) {
            $iter = $value->getIterator();
        } elseif ($value instanceof \Iterator) {
            $iter = $value;
        } else {
            return null;
        }

        return $this->doGetChildren($iter);
    }

    protected function doGetChildren(\Traversable $iter): \RecursiveIterator
    {
        return new self($iter);
    }
}
