<?php
declare(strict_types=1);
/*
 * This file is part of "irstea/ng-model-generator-bundle".
 *
 * "irstea/ng-model-generator-bundle" generates Typescript interfaces for Angular using api-platform metadata.
 * Copyright (C) 2018-2021 IRSTEA
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
 * PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License and the GNU
 * Lesser General Public License along with this program. If not, see
 * <https://www.gnu.org/licenses/>.
 */

namespace Irstea\NgModelGeneratorBundle\Iterators;

/**
 * Class IteratorBuilder.
 */
final class IteratorBuilder implements \IteratorAggregate
{
    /** @var mixed */
    private $source;

    /** @var bool */
    private $unique = false;

    /** @var int|false */
    private $recurseMode = false;

    /** @var callable|null */
    private $recurseFilter;

    /** @var callable|null */
    private $where;

    /**
     * IteratorBuilder constructor.
     *
     * @param mixed $source
     */
    private function __construct($source)
    {
        $this->source = $source;
    }

    /**
     * @return IteratorBuilder
     */
    public function unique(): self
    {
        $clone = clone $this;
        $clone->unique = true;

        return $clone;
    }

    /**
     * @return IteratorBuilder
     */
    public function recurse(int $mode): self
    {
        if ($mode === $this->recurseMode) {
            return $this;
        }
        $clone = clone $this;
        $clone->recurseMode = $mode;

        return $clone;
    }

    /**
     * @return IteratorBuilder
     */
    public function recurseWhere(int $mode, callable $filter): self
    {
        if ($mode === $this->recurseMode && $filter === $this->recurseFilter) {
            return $this;
        }
        $clone = clone $this;
        $clone->recurseMode = $mode;
        $clone->recurseFilter = $filter;

        return $clone;
    }

    /**
     * @return IteratorBuilder
     */
    public function where(callable $where): self
    {
        $clone = clone $this;
        $clone->where = $where;

        return $clone;
    }

    /**
     * {@inheritdoc}
     */
    public function getIterator()
    {
        $iter = $this->getBaseIterator();

        if ($this->recurseMode !== false) {
            if ($this->recurseFilter) {
                $iter = new CallbackFilterRecursorIterator($iter, $this->recurseFilter);
            } else {
                $iter = new RecursorIterator($iter);
            }
        }

        if ($this->unique) {
            if ($this->recurseMode !== false) {
                $iter = new \RecursiveCallbackFilterIterator($iter, new UniqueFilter());
            } else {
                $iter = new \CallbackFilterIterator($iter, new UniqueFilter());
            }
        }

        if ($this->recurseMode !== false) {
            $iter = new \RecursiveIteratorIterator($iter, $this->recurseMode);
        }

        if ($this->where) {
            $iter = new \CallbackFilterIterator($iter, $this->where);
        }

        return $iter;
    }

    /**
     * {@inheritdoc}
     */
    private function getBaseIterator()
    {
        if ($this->source instanceof \Iterator) {
            return $this->source;
        }
        if ($this->source instanceof \IteratorAggregate) {
            return $this->source->getIterator();
        }

        return new \ArrayIterator($this->source);
    }

    /**
     * @param mixed $source
     *
     * @return IteratorBuilder
     */
    public static function from($source): self
    {
        return new self($source);
    }
}
