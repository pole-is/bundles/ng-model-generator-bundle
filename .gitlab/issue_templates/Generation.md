### Code PHP des entités impliquées

```php
/**
 * @API\ApiResource()
 * @ORM\Entity()
 */
class Person
{
    /**
     * @var UuidInterface
     * @ORM\Id()
     * @ORM\Column()
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(unique=true)
     */
    private $name;

    /* getters, setters */
}
```

### Code Typescript attendu

```typescript
export interface Person {
  readonly '@id': IRI<Person>;
  id: string;
  readonly '@type': 'Person';
  name?: string;
}
```

### Code Typescript généré ou message d'erreur

```typescript
export interface Person {
  readonly '@id': IRI<Person>;
  id: string;
  readonly '@type': 'Person';
  name: string;
}
```
